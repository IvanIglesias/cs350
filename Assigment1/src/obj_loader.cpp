/**
* @file		obj_loader.cpp
* @date 	2/11/2019
* @author	Ivan Iglesias
* @par		Login: ivan.iglesias
* @par		Course: CS350
* @par		Assignment #2
* @brief 	Implementation of obj loader functions
*
* Hours spent on this assignment: 20h
*
*/
#include "obj_loader.h"
#include "pch.h"
#include <cstring>

/**
* @brief 	function to load an object
* @param	out_vertices		loaded vertices
* @param	out_vert_index		loaded indeces
* @param	out_normals		loaded normals
* @param	filename		file name
*/
void Load_Obj(std::string filename,std::vector < glm::vec3 > & out_vertices,
	std::vector < short unsigned > & out_vert_index,
	std::vector < glm::vec3 > & out_normals)
{

	FILE * file = fopen(filename.c_str(), "r");
	if (file == NULL) {
		printf("Impossible to open the file !\n");
		return;
	}

	while (1) {

		char lineHeader[128];
		// read the first word of the line
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break; // EOF = End Of File. Quit the loop.

		if (strcmp(lineHeader, "v") == 0) {
			glm::vec3 vertex;
			res = fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			out_vertices.push_back(vertex);

		}	
		else if (strcmp(lineHeader, "vn") == 0) {
			glm::vec3 normal;
			res = fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			out_normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0) {
			std::string vertex1, vertex2, vertex3;
			int vertexIndex1;
			int vertexIndex2;
			int vertexIndex3;

			int matches = fscanf(file, "%d %d %d\n", &vertexIndex1, &vertexIndex2, &vertexIndex3);
			if (matches != 3) {
				printf("File can't be read by our simple parser : ( Try exporting with other options\n");
				return;
			}
			out_vert_index.push_back(static_cast<unsigned short>(vertexIndex1 - 1));
			out_vert_index.push_back(static_cast<unsigned short>(vertexIndex2 - 1));
			out_vert_index.push_back(static_cast<unsigned short>(vertexIndex3 - 1));
		}
	}	
}
