/**
* @file		renderer.cpp
* @date 	1/18/2019
* @author	Ivan Iglesias
* @par		Login: ivan.iglesias
* @par		Course: CS350
* @par		Assignment #1
* @brief 	Implementation of renderer
*
* Hours spent on this assignment: 10h
*
*/
#include "pch.h"
#include "renderer.h"
#include "obj_loader.h"
#include "transform.h"
#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "geometry.h"
#include "bounding_volume_hierarchy.h"
#include <functional>
#include "octree.h"

renderer render;

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_buttom_callback(GLFWwindow* window, int button, int action, int mods);

//window size

const unsigned int SCR_WIDTH = 1280;
const unsigned int SCR_HEIGHT = 720;

float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

//Showing object

// timing
float deltaTime = 0.0f;	// time between current frame and last frame
float lastFrame = 0.0f;

//shaders
const char *vertexShaderSource = "#version 400 core\n"
"layout (location = 0) in vec3 aPos;\n"
"uniform mat4 MVP;\n"
"void main()\n"
"{\n"
"   gl_Position = MVP * vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
"}\0";
const char *fragmentShaderSource = "#version 400 core\n"
"out vec4 FragColor;\n"
"uniform vec4 vertColor;\n"
"void main()\n"
"{\n"
"   FragColor = vertColor;\n"
"}\n\0";
GLFWwindow* window;
int shaderProgram;

std::string m_win_name;
ImGuiWindowFlags m_flags;
/**
* @brief 	function to init imgui
* @return	if failed or succes 
*/
bool InitImGui()
{
	//call imgui functions
	ImGui::CreateContext();
	ImGui::StyleColorsClassic();
	if (!ImGui_ImplGlfw_InitForOpenGL(window, true))
		return false;

	const char * glsl_version = "#version 130";

	if (!ImGui_ImplOpenGL3_Init(glsl_version))
		return false;


	m_win_name = "Editor Render";
	//NOt moving windons!!!
	m_flags = ImGuiWindowFlags_NoMove;

	return true;
}
/**
* @brief 	function to render imgui
*/
void render_imgui()
{
	ImGui::Begin(m_win_name.c_str(), nullptr, m_flags);


	// List Object center
	const char* listbox_items[] = { "bunny", "gourd"};	
	auto prev_num = render.object_num;
	//const char* listbox_items[] = { "bunny1", "bunny2", "bunny3", "bunny4", "bunny5" };
	ImGui::ListBox("Central Object", &render.object_num, listbox_items, IM_ARRAYSIZE(listbox_items), IM_ARRAYSIZE(listbox_items));
	if(prev_num != render.object_num)
		render.create_kdtree = true;

	//Central object translation, rotation, scale

	/*if (ImGui::Button("Add Object"))
		render.add_object();*/

	//ImGui::InputInt("Object number", &render.tot_obj_num);

	ImGui::InputInt("Tree Level", &render.tree_level);

	auto prev_depth = render.max_depth;
	ImGui::InputInt("Max Depth", &render.max_depth);

	if (render.max_depth != prev_depth)	
		render.create_kdtree = true;
	

	
/*	if (ImGui::Button("Random Scene"))
		render.generate_objects();*/
	
	/*ImGui::Checkbox("Freeze frustum", &render.freeze_frustum);*/


	render.imgui_hovered = ImGui::IsWindowHovered();		

	if (render.object_num != -1)
	{
		//if this returns true the objects has been move
		bool bool_pos = ImGui::DragFloat3("Pos", &render.objects_render[render.object_num]->TransformData.Position.x, 0.01f);
		//bool bool_rot = ImGui::DragFloat3("Rot", &render.objects_render[render.object_num]->TransformData.Rotate.x, 0.01f);
		bool bool_scale = ImGui::DragFloat3("Scale", &render.objects_render[render.object_num]->TransformData.Scale.x, 0.01f);

		if (bool_pos /*|| bool_rot */|| bool_scale)//delete object from the octree and reinsert it
		{
			/*render.my_octree.delete_object(render.objects_render[render.object_num]);
			render.my_octree.insert_object(render.objects_render[render.object_num], 5);*/

		}

		/*if (ImGui::Button("Reset"))
		{
			render.objects_render[render.object_num]->TransformData.Reset();
		}*/
	}
	
	// Simplified one-liner Combo() API, using values packed in a single constant string
	//ImGui::Combo("Bounding Type", &render.bounding_type, "AABB\0SPHERE\0");
	
	ImGui::End();


}
/**
* @brief 	function to update imgui
*/
void update_imgui()
{
	//call imgui funcions on update
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
	//ImGui::ShowDemoWindow();
	render_imgui();
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}
/**
* @brief 	function to shutdown imgui
*/
void shutdown_imgui()
{
	//shutdown imgui
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
}
/**
* @brief 	function to create window
*/
void WindowCreation()
{
	// glfw window creation
	window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "CS350", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();		
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetMouseButtonCallback(window, mouse_buttom_callback);

}
/**
* @brief 	function to init glfw
*/
void InitGlfw()
{
	// glfw: initialize and configure
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	WindowCreation();
}
/**
* @brief 	function to load opengl
*/
void LoadOpenGL()
{
	// glad: load all OpenGL function pointers
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;		
	}
}
/**
* @brief 	function to compile shaders
*/
void BuildAndCompileShaders()
{
	// build and compile our shader program
	int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);
	// check for shader compile errors
	int success;
	char infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
	// fragment shader
	int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);
	// check for shader compile errors
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
	// link shaders
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);
	// check for linking errors
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}
/**
* @brief 	function to load all the meshes
*/
void renderer::load_all_meshes()
{
	objects_mesh.push_back({ "../meshes/bunny.obj" });
	objects_mesh.push_back({ "../meshes/cube.obj" });
	objects_mesh.push_back({ "../meshes/cylinder.obj" });
	objects_mesh.push_back({ "../meshes/gourd.obj" });
	objects_mesh.push_back({ "../meshes/icosahedron.obj" });
	objects_mesh.push_back({ "../meshes/octohedron.obj" });
	objects_mesh.push_back({ "../meshes/quad.obj" });
	objects_mesh.push_back({ "../meshes/segment.obj" });
	objects_mesh.push_back({ "../meshes/sphere.obj" });
	objects_mesh.push_back({ "../meshes/triangle.obj" });	
	generate_objects();

}
/**
* @brief 	main function of the renderer
* @return	if failed or succes
*/
bool renderer::entry_point() {
	
	//cal init and load functions
	InitGlfw();	
	LoadOpenGL();
	InitImGui();
	BuildAndCompileShaders();

	load_all_meshes();
	
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	
	glUseProgram(shaderProgram);
	GLint mvp_location = glGetUniformLocation(shaderProgram, "MVP");
	GLint vertColor_location = glGetUniformLocation(shaderProgram, "vertColor");

	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);

	// render loop
	while (!glfwWindowShouldClose(window))
	{
		//frame time
		float currentFrame = (float)glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;
		// input
		processInput(window);		
		// render
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		// draw our first triangle

		auto Viewport = cam.GetViewMatrix();
		auto Projection = cam.ProjectionMatrix();

		//update data of all the objects 
		for (auto & obj : objects_render)
			obj->TransformData.UpdateMatrices();
		//objects_render[0].TransformData.Position = mouse_pos;

		for (unsigned i = 0; i < objects_render.size(); i++)
		{
			std::vector < vec3 > vert_modified;

			if (bounding_type == 1 || aabb_type != 1)
			{
				//Update per frame		
				auto size_temp = objects_mesh[objects_render[i]->mesh_index].out_vertices.size();
				auto temp_vert = objects_mesh[objects_render[i]->mesh_index].out_vertices;
				auto model_mat = objects_render[i]->TransformData.Model;
				for (unsigned j = 0; j < size_temp; j++)
				{
					vert_modified.push_back(glm::vec3(model_mat * glm::vec4(temp_vert[j], 1.0f)));
				}
			}

			switch (bounding_type)
			{
				//aabb
			case 0:
			{
				if (aabb_type == 0)
				{
					auto new_aabb = aabb_from_points(vert_modified);
					debug_render_aabb(new_aabb, objects_render[i]->color_aabb);
				}
				else if (aabb_type == 1) //use matrix
				{
					auto tran_aabb = aabb_from_aabb_and_transform(objects_render[i]->bounding_aabb,
						objects_render[i]->TransformData.Model);
					objects_render[i]->transform_aabb = tran_aabb;
				}
			}
			break;
			case 1:	//sphere
			{
				if (sphere_type == 0) //centroid
				{
					debug_render_sphere(sphere_from_points_centroid(vert_modified), objects_render[i]->color_aabb);
				}
				else if (sphere_type == 1)
				{
					debug_render_sphere(sphere_from_points_ritter(vert_modified), objects_render[i]->color_aabb);
				}
				else if (sphere_type == 2)
				{
					debug_render_sphere(sphere_from_points_iterative(vert_modified, 4, 0.95f), objects_render[i]->color_aabb);
				}

			}
			break;
			default:
				break;
			}
		}		
		/*if(create_octree)
		{
			for (auto & obj : objects_render)
				my_octree.insert_object(obj, 5);
			create_octree = false;
		}
		if (insert_octree_last)
		{
			my_octree.insert_object(objects_render.back(), 5);
			insert_octree_last = false;
		}


		
		//my_octree.delete_object(&objects_render[0]);
		//Iterate through
		std::function<void(octree<Model>::node * Pnode)> octree_add
			= [&](octree<Model>::node * Pnode)
		{
			if (Pnode == nullptr) //no octree
				return;
			for (unsigned i = 0; i < 8; i++)
			{
				if (Pnode->children[i] != nullptr)
					octree_add(Pnode->children[i]);
			}			
			debug_render_aabb(Pnode->bounding_volume, default_aabb_color);
		};

		octree_add(my_octree.root());
		if (!freeze_frustum)
		{
			cam_frustrum = { Projection * Viewport };
			cam.far_plane = 80;
		}else
			cam.far_plane = 150;
		
		unsigned frustum_order[] = { 0,1,1,3,3,2,2,0,0,4,1,5,3,7,2,6,4,5,5,7,7,6,6,4 };
		auto points_frustum = cam_frustrum.get_points();
		for (unsigned i = 0; i < 12; i++)
			debug_render_segment(points_frustum[frustum_order[i * 2]], points_frustum[frustum_order[i * 2 + 1]], glm::vec4(0, 0, 1, 1));

		std::vector<Model*> final_objs_render;
		my_octree.octree_query(cam_frustrum, final_objs_render);
		//render aabb of the objects visualized
		for(unsigned i = 0; i < final_objs_render.size(); i++)
			debug_render_aabb(final_objs_render[i]->transform_aabb, final_objs_render[i]->color_aabb);		
		*/
		//KDTrees

		std::vector<kdtree::KDNode *> nodes_kdtree;

		//add big aabb
		debug_render_aabb(objects_render[object_num]->transform_aabb, default_aabb_color);

		std::function<void(kdtree::KDNode * Pnode, int level)> kdtree_add
			= [&](kdtree::KDNode * Pnode, int level)
		{
			if (Pnode == nullptr || level <= 0) //no octree
				return;
			
			//create aabb
			auto max = Pnode->node_aabb.max_point;
			auto min = Pnode->node_aabb.min_point;

			if (Pnode->plane_position > max[Pnode->plane_axis])
				max[Pnode->plane_axis] = Pnode->plane_position;

			max[Pnode->plane_axis] = min[Pnode->plane_axis] = Pnode->plane_position;

			aabb plane_aabb(min, max);
			auto tran_aabb = aabb_from_aabb_and_transform(plane_aabb,
				objects_render[object_num]->TransformData.Model);

			glm::vec4 color_act;
			switch (Pnode->plane_axis)
			{
			case 0:
				color_act = glm::vec4(1.0f, 0, 0, 1.0f);
				break;
			case 1:
				color_act = glm::vec4(0, 1.0f, 0, 1.0f);
				break;
			case 2:
				color_act = glm::vec4(0, 0, 1.0f, 1.0f);
				break;
			default:
				break;
			}
			//Iterate through the childs
			debug_render_aabb(tran_aabb, color_act);

			if (Pnode->child[0] != nullptr)
			{				
				kdtree_add(Pnode->child[0], level - 1);
			}
			if (Pnode->child[1] != nullptr)
			{
				kdtree_add(Pnode->child[1], level - 1);
			}			
		};
		//load gourd and bunny
		if(create_kdtree)
		{
			if(object_num == 1)
				tree_gourd.build(load_triangles_from_obj("../meshes/gourd.obj"), 7);
			else
				tree_gourd.build(load_triangles_from_obj("../meshes/bunny.obj"), 7);

			create_kdtree = false;
		}

		kdtree_add(tree_gourd.get_root(), tree_level);

		//objects_render
		/*for (unsigned i = 0; i < objects_render.size(); i++)
		{*/
			//final_objs_render[i]->TransformData.UpdateMatrices();

			auto MVP = Projection * Viewport * objects_render[object_num]->TransformData.Model;

			//set the mvp matrix
			glUniformMatrix4fv(mvp_location, 1, GL_FALSE, &MVP[0][0]);
			//binding vao
			glBindVertexArray(objects_mesh[objects_render[object_num]->mesh_index].vao);

			//draw model
			vec4 color_first(0.5, 0.7, 0.1, 1.0);
			glUniform4f(vertColor_location, color_first.x, color_first.y, color_first.z, color_first.w);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(objects_mesh[objects_render[object_num]->mesh_index].out_index_vert.size()), GL_UNSIGNED_SHORT, 0);

			//draw lines
			vec4 color_second(0.25);
			glUniform4f(vertColor_location, color_second.x, color_second.y, color_second.z, color_second.w);
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(objects_mesh[objects_render[object_num]->mesh_index].out_index_vert.size()), GL_UNSIGNED_SHORT, 0);

			glBindVertexArray(0); // no need to unbind it every time 

		//}
		//glDisable(GL_BLEND);

			//
			
		//RenderBoundings
		for (unsigned i = 0; i < bounding_models.size(); i++)
		{
			bounding_models[i].TransformData.UpdateMatrices();

			auto MVP = Projection * Viewport * bounding_models[i].TransformData.Model;
			glBindVertexArray(objects_mesh[bounding_models[i].mesh_index].vao);

			glUniformMatrix4fv(mvp_location, 1, GL_FALSE, &MVP[0][0]);
			if (i != 0)
			{
				//bind vao
				glEnable(GL_CULL_FACE);
				glCullFace(GL_BACK);
				//draw
				vec4 color_first = bounding_models[i].color_aabb;//vec4(0.2, 0.1, 0.1, 0.75);
				glUniform4f(vertColor_location, color_first.x, color_first.y, color_first.z, color_first.w);
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(objects_mesh[bounding_models[i].mesh_index].out_index_vert.size()), GL_UNSIGNED_SHORT, 0);

				glDisable(GL_CULL_FACE);

			}

			vec4 color_second = vec4(0.25);
			glUniform4f(vertColor_location, color_second.x, color_second.y, color_second.z, color_second.w);
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(objects_mesh[bounding_models[i].mesh_index].out_index_vert.size()), GL_UNSIGNED_SHORT, 0);

			//unbind
			glBindVertexArray(0); 
		}
		//glEnable(GL_BLEND);


		bounding_models.clear();		
		//update imgui
		update_imgui();

		// glfw: swap buffers and poll IO events
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	//stop program
	glUseProgram(0);

	// glfw: terminate
	shutdown_imgui();
	glfwTerminate();
	return 0;
}





/**
* @brief 	function to process the input from glfw window
* @param	window		pointer to window
*/
void renderer::processInput(GLFWwindow *window_input)
{
	//esc input
	if (glfwGetKey(window_input, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window_input, true);

	//Cam input 
	if (glfwGetKey(window_input, GLFW_KEY_W) == GLFW_PRESS)
	{
		if (glfwGetKey(window_input, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
			cam.ProcessKeyboard(FORWARD, deltaTime * 5);
		else
			cam.ProcessKeyboard(FORWARD, deltaTime);
	}

	if (glfwGetKey(window_input, GLFW_KEY_S) == GLFW_PRESS)
	{
		if (glfwGetKey(window_input, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
			cam.ProcessKeyboard(BACKWARD, deltaTime * 5);
		else
			cam.ProcessKeyboard(BACKWARD, deltaTime);
	}

	if (glfwGetKey(window_input, GLFW_KEY_A) == GLFW_PRESS)
	{
		if (glfwGetKey(window_input, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
			cam.ProcessKeyboard(LEFT, deltaTime * 5);
		else
			cam.ProcessKeyboard(LEFT, deltaTime);
	}

	if (glfwGetKey(window_input, GLFW_KEY_D) == GLFW_PRESS)
	{
		if (glfwGetKey(window_input, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
			cam.ProcessKeyboard(RIGHT, deltaTime * 5);
		else
			cam.ProcessKeyboard(RIGHT, deltaTime);
	}

	//delete object
/*	if (glfwGetKey(window_input, GLFW_KEY_DELETE) == GLFW_PRESS && object_num != -1)
	{
		my_octree.delete_object(objects_render[object_num]);
		delete objects_render[object_num];
		objects_render.erase(objects_render.begin() + object_num);
		object_num = -1;
		prev = -1;
		selecting = -1;
	}
	*/
	//compute mouse position in world
	glfwGetCursorPos(window_input, &mouse_xpos, &mouse_ypos);
	auto pos_x_view = mouse_xpos / SCR_WIDTH;
	auto pos_y_view = mouse_ypos / SCR_HEIGHT;
	//NDC
	auto ndc_x = (pos_x_view * 2) - 1;
	auto ndc_y = (pos_y_view * 2) - 1;
	//std::cout << "=================================" << std::endl;
	//std::cout << "ndc_x x" << ndc_x << std::endl;
	//std::cout << "ndc_x y" << ndc_y << std::endl;
	//Viewport
	vec4 ndc_vec = { ndc_x , -ndc_y, 1, 1 };	
	auto inv_view = glm::inverse( cam.ProjectionMatrix() * cam.GetViewMatrix());
	vec4 world_pos = inv_view * ndc_vec;
	//perspective division
	world_pos /= world_pos.w;

	//std::cout << "=================================" << std::endl;
	//std::cout << "world x" << world_pos.x << std::endl;
	//std::cout << "world y" << world_pos.y << std::endl;
	//std::cout << "world z" << world_pos.z << std::endl;
	/*mouse_pos = { world_pos.x, world_pos.y, world_pos.z };

	//compute ray between mouse point and camara center
	ray cam_point{ cam.Position, glm::normalize(mouse_pos - cam.Position)};

	//compute intersection between all objects and ray

	unsigned i = 0;
	float min_inter = FLT_MAX;
	int pos = -1;
	for (; i < objects_render.size(); i++)
	{
		//Intersection found

	//	auto result_aabb = aabb_from_aabb_and_transform(objects_render[i].bounding_aabb, objects_render[i].TransformData.Model);
		auto inter = intersection_ray_aabb(cam_point, objects_render[i]->transform_aabb);
		if (inter > 0.0)
		{
			if (inter < min_inter)
			{
				min_inter = inter;
				pos = i; 
			}			
		//	objects_render[i].TransformData.Position = mouse_pos;
		//	break;
		}
	}

	if (pos != -1)//intersection found 
	{
		selecting = pos;
		if (prev!= -1 && prev != pos && prev != object_num)
		{
			objects_render[prev]->color_aabb = default_aabb_color;
		}
		prev = pos;
		objects_render[pos]->color_aabb = {0,1,0,1};
	}
	else
	{
		if (prev != -1 && prev != object_num)//change color to original
		{
			objects_render[prev]->color_aabb = default_aabb_color;
			prev = -1;
			selecting = -1;
		}
	}
	*/
	if (rotate)
	{
		double xpos, ypos;

		glfwGetCursorPos(window_input, &xpos, &ypos);

		if (firstMouse)
		{
			lastX = (float)xpos;
			lastY = (float)ypos;
			firstMouse = false;
		}

		float xoffset = static_cast<float>(xpos - lastX);
		float yoffset = static_cast<float>(lastY - ypos); // reversed since y-coordinates go from bottom to top

		lastX = static_cast<float>(xpos);
		lastY = static_cast<float>(ypos);

		render.cam.ProcessMouseMovement(xoffset, yoffset);
	}


}
/**
* @brief 	function to generate the objects
*/
void renderer::generate_objects()
{
	/*if(my_octree.root())
		my_octree.delete_octree();

	for (unsigned i = 0; i < objects_render.size(); i++)
		delete objects_render[i];
	objects_render.clear();
	create_octree = true;*/

	objects_render.push_back(new Model);
	glm::vec3 pos(0);
	objects_render.back()->TransformData.start_position = objects_render.back()->TransformData.Position + pos;
	objects_render.back()->TransformData.Position += pos;
	objects_render.back()->mesh_index = 0;
	objects_render.back()->bounding_aabb = aabb_from_points(objects_mesh[0].out_vertices);
	objects_render.back()->color_aabb = default_aabb_color;

	objects_render.push_back(new Model);
	objects_render.back()->TransformData.start_position = objects_render.back()->TransformData.Position + pos;
	objects_render.back()->TransformData.Position += pos;
	objects_render.back()->mesh_index = 3;
	objects_render.back()->bounding_aabb = aabb_from_points(objects_mesh[3].out_vertices);
	objects_render.back()->color_aabb = default_aabb_color;

	object_num = 1;
	//insert_octree_last = true;



	/*for (int i = 0; i < tot_obj_num; i++)
	{
		add_object();
	}*/
}
/**
* @brief 	function to add object
*/
void renderer::add_object()
{

	objects_render.push_back(new Model);		//
	//glm::vec3 pos{ rand() % (3 * tot_obj_num), rand() % (3 * tot_obj_num) , rand() % (3 * tot_obj_num) };
	glm::vec3 pos(0);
	objects_render.back()->TransformData.start_position = objects_render.back()->TransformData.Position + pos;
	objects_render.back()->TransformData.Position += pos;
	objects_render.back()->mesh_index = 0;
	objects_render.back()->bounding_aabb = aabb_from_points(objects_mesh[0].out_vertices);
	objects_render.back()->color_aabb = default_aabb_color;
	insert_octree_last = true;

}
/**
* @brief 	function to change glfw window size
* @param	window		pointer to window
*/
void framebuffer_size_callback(GLFWwindow* window_framebuffer, int width, int height)
{
	//change viewport
	glViewport(0, 0, width, height);
}

void mouse_buttom_callback(GLFWwindow* window_mouse, int button, int action, int mods)
{

	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
	{	
		//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		render.rotate = true;
	}
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
	{
		//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		firstMouse = true;
		render.rotate = false;
	}

	/*if (render.imgui_hovered)
		return;
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		if (render.selecting != -1 && render.selecting != render.object_num)
		{			
			if(render.object_num != -1)
				render.objects_render[render.object_num]->color_aabb = render.default_aabb_color;
			render.object_num = render.selecting;
			render.objects_render[render.object_num]->color_aabb = { 0,0,1,1 };
		}
		else
		{

			if (render.object_num != -1)
				render.objects_render[render.object_num]->color_aabb = render.default_aabb_color;
			render.object_num = -1;
		}
	
	}*/


}
/**
* @brief 	function to call when debug rendering the bounding volume sphere
* @param	color		color of the sphere
* @param	sphere		sphere to render
*/
void renderer::debug_render_sphere(const sphere & sphere, const vec4 & color)
{
	//sphere.radius
	//make the model matrix with the transform part
	Model sphere_model;
	sphere_model.TransformData.Scale = vec3(sphere.radius, sphere.radius, sphere.radius);
	sphere_model.TransformData.Position = sphere.center;
	sphere_model.color_aabb = color;
	sphere_model.mesh_index = 8;
	bounding_models.push_back(sphere_model);
}
/**
* @brief 	function to call when debug rendering the bounding volume aabb
* @param	color		color of the aabb
* @param	aabb		aabb to render
*/
void renderer::debug_render_aabb(const aabb & aabb, const vec4 & color)
{
	Model aabb_model;
	//set the aabb model and mesh
	aabb_model.TransformData.Scale = (aabb.max_point - aabb.min_point) / 2.0f;
	aabb_model.TransformData.Position = aabb.get_center();
	aabb_model.color_aabb = color;
	aabb_model.mesh_index = 1;
	aabb_model.bounding_aabb = aabb;
	bounding_models.push_back(aabb_model);
}
void renderer::debug_render_segment(const vec3 & start, const vec3 & end, const vec4 & color)
{
	Model segment_model;
	//pos start + (end - start) / 2
	//scale (end - start)

	segment_model.TransformData.Scale = end - start;
	segment_model.TransformData.Position = start + (end - start) / 2.0f;
	segment_model.color_aabb = color;
	segment_model.mesh_index = 7;	
	bounding_models.push_back(segment_model);
	

}
void renderer::debug_render_plane(const plane & plane, const vec4 & color)
{
	/*Model plane_model;
	
	//set the aabb model and mesh
	plane_model.TransformData.Scale = (aabb.max_point - aabb.min_point) / 2.0f;
	plane_model.TransformData.Position = aabb.get_center();
	plane_model.color_aabb = color;
	plane_model.mesh_index = 6;
	bounding_models.push_back(plane_model);*/

}
/**
* @brief 	main function of the demo
* @param	argc		argument
* @param	argv		argument
* @return	if posible to render
*/
int main_demo(int argc, char** argv) {
	
	return render.entry_point() ? 0 : -1;
}