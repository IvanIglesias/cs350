/**
* @file		octree.h
* @date 	3/20/2019
* @author	Ivan Iglesias
* @par		Login: ivan.iglesias
* @par		Course: CS350
* @par		Assignment #4
* @brief 	Implementation of octree
*
* Hours spent on this assignment: 20h
*
*/
#pragma once
#include "pch.h"
#include "shapes.h"
#include "geometry.h"
#include <functional>
#include "model.h"


template<typename T>
class octree {
public:
	/**
	*
	*/
	struct node {

		/**
		* If leaf, the pointer to the contained objects
		*/
		std::vector<T*> objects{};
		/**
		* Bounding volume of the current node
		*/
		aabb bounding_volume{};
		/**
		* Pointers to the children
		*/
		std::array<node*, 8> children = {nullptr};
	};

private:
	bool delete_object_recursive(T * pObject, node * & pTree);
	void delete_octree_recursive(node* &pTree);
public:	
	node* m_root = nullptr;
	std::vector<T*> inserted_objects{};
	node * root() { return m_root; }
	glm::vec3 size;
	int stop_depth;
	void insert_object(T * pObject, int stopDepth, node * pTree = nullptr, bool reinserted = false);
	const aabb& get_bounding_volume(const aabb& lhs);
	const aabb& get_bounding_volume(const Model * lhs);
	void delete_octree(bool reinsert = false);
	const Model *get_model(const Model * lhs);
	void delete_object(T * pObject);

	void octree_query(frustum& cam_frustrum, std::vector<Model*> & final_render_objects, node * pTree = nullptr);
	~octree() { delete_octree(); }
};
/**
* @brief 	function to delete the contents of the octree
* @param	reinsert to know if we are reinserting the tree
*/
template<typename T>
void octree<T>::delete_octree(bool reinsert)
{
	if (m_root == nullptr)
		return;
	else
		delete_octree_recursive(m_root);
	if (!reinsert)
		inserted_objects.clear();

}
/**
* @brief 	function to delete the octree recusively
* @param	pTree pointer to the tree to start from
*/
template<typename T>
void octree<T>::delete_octree_recursive(node *& pTree)
{
	//add all the children of the actual node
	for (unsigned i = 0; i < 8; i++)
	{
		if (pTree->children[i] != nullptr)
		{
			delete_octree_recursive(pTree->children[i]);
		}
	}

	delete pTree;
	pTree = nullptr;
}

/**
* @brief 	return the aabb
* @param	lhs	object to remove
* @return	aabb result
*/
template<typename T>
const aabb & octree<T>::get_bounding_volume(const aabb & lhs)
{
	return lhs;
}
/**
* @brief 	returns the aabb
* @param	model pointer
* @return	aabb result
*/
template<typename T>
const aabb & octree<T>::get_bounding_volume(const Model * lhs)
{
	return lhs->transform_aabb;
}
/**
* @brief 	returns the model pointer
* @param	model pointer
* @return	the pointer to the model
*/
template<typename T>
const Model * octree<T>::get_model(const Model * lhs)
{
	return lhs;
}
/**
* @brief 	function to delete_object an object
* @param	pObject object to delete
*/
template<typename T>
void octree<T>::delete_object(T * pObject)
{
	if (m_root == nullptr)
		return;
	//remove from the inserted objects vector
	unsigned i;
	for (i = 0; i < inserted_objects.size(); i++)
	{
		if (inserted_objects[i] == get_model(pObject))
			break;
	}
	inserted_objects.erase(inserted_objects.begin() + i);
	//Iterate the tree cursively to find the object and delete it

	delete_object_recursive(pObject, m_root);
}
/**
* @brief 	function to delete the object
* @param	pObject object to delete
* @param	pTree tree to iterate
* @return	if manage to delete
*/
template<typename T>
bool octree<T>::delete_object_recursive(T * pObject, node * & pTree)
{
	//check with the objects at this level
	for (unsigned i = 0; i < pTree->objects.size(); i++)
	{
		//found object
		if (pTree->objects[i] == get_model(pObject))
		{
			//delete from the vector
			pTree->objects.erase(pTree->objects.begin() + i);
			bool all_out = true;
			for (unsigned j = 0; j < 8; j++)
			{
				if (pTree->children[j] != nullptr)
					all_out = false;
			}
			if (all_out && pTree->objects.empty())
			{
				delete pTree;
				pTree = nullptr;
			}
			return true;
		}
	}

	for (unsigned i = 0; i < 8; i++)
	{
		if (pTree->children[i] != nullptr)
		{
			if (delete_object_recursive(pObject, pTree->children[i]))
			{
				bool all_out = true;
				for (unsigned j = 0; j < 8; j++)
				{
					if (pTree->children[j] != nullptr)					
						all_out = false;					
				}
				//delete this child 
				if (all_out && pTree->objects.empty())
				{
					delete pTree;
					pTree = nullptr;
				}

				return true;
			}
		}
	}

	return false;
}
/**
* @brief 	function to delete the object
* @param	pObject object to delete
* @param	pTree tree to iterate
* @return	if manage to delete
*/
template<typename T>
void octree<T>::octree_query(frustum & cam_frustrum, std::vector<Model*>& final_render_objects, node * pTree)
{	
	//no octree
	if (m_root == nullptr)
		return;
	//test collision against frustum
	if (pTree == nullptr)
		pTree = m_root;	
	intersection_type type = intersection_frustum_aabb(cam_frustrum, pTree->bounding_volume);
	if (intersection_type::OUTSIDE == type || intersection_type::COPLANAR == type)
	{
		return;
	}
	else //we insert the objects at this level 
	{
		for (unsigned i = 0; i < pTree->objects.size(); i++)
		{
			//if aabb is overlapping check objects individualy
			if (type == intersection_type::OVERLAPS)
			{
				intersection_type type_2 = intersection_frustum_aabb(cam_frustrum, get_bounding_volume(pTree->objects[i]));
				if (intersection_type::OUTSIDE == type_2 || intersection_type::COPLANAR == type_2)
					continue;
			}
			
			final_render_objects.push_back(pTree->objects[i]);
			
		}
	}
	for (unsigned i = 0; i < 8; i++)
	{

		if (pTree->children[i] != nullptr)
		{
			octree_query(cam_frustrum, final_render_objects, pTree->children[i]);
		}
	}
}
/**
* @brief 	function to delete the object
* @param	pObject object to insert
* @param	stopDepth depth to stop
* @param	pTree pointer to the tree
*/
template<typename T>
void octree<T>::insert_object(T * pObject, int stopDepth, node * pTree, bool reinserted)
{
	if (stopDepth < 0)
	{
		if (m_root)
		{
			//insert at this level and return
			pTree->objects.push_back(pObject); 
			if (!reinserted)
				inserted_objects.push_back(pObject);
			return;
		}
	}
	//we add the object as the root of the tree
	if(m_root == nullptr)
	{
		m_root = new node;		
		m_root->bounding_volume = get_bounding_volume(pObject);
		m_root->objects.push_back(pObject); 
		inserted_objects.push_back(pObject);
		return;
	}

	if (pTree == nullptr)
		pTree = m_root;	
	
	int index = 0;
	int straddle = 0;	

	//check maximum and minimum size in all the axises if on the root node
	if (!reinserted && pTree == m_root)
	{
		for (unsigned i = 0; i < 3; i++)
		{
			//if this happens we resize
			if (pTree->bounding_volume.max_point[i] < get_bounding_volume(pObject).max_point[i]
				|| pTree->bounding_volume.min_point[i] > get_bounding_volume(pObject).min_point[i])
			{
				
				//call aabb from points
				aabb big_aabb = aabb_from_points({pTree->bounding_volume.max_point, get_bounding_volume(pObject).max_point, 
					pTree->bounding_volume.min_point, get_bounding_volume(pObject).min_point });
				big_aabb.max_point += glm::vec3(0.05f);
				big_aabb.min_point -= glm::vec3(0.05f);
				//we store all the objects in the tree
				delete_octree(true);
				//create new root
				m_root = new node;
				m_root->bounding_volume = big_aabb;
				m_root->objects.push_back(pObject);

				inserted_objects.push_back(pObject);
				//we resinsert all the objects
				for (unsigned j = 0; j < inserted_objects.size() - 1; j++)
					insert_object(inserted_objects[j], stopDepth - 1, m_root, true);
				return;
			}
		}
	}
	for(unsigned i = 0; i < 3; i++)
	{
		float delta = get_bounding_volume(pObject).get_center()[i] - pTree->bounding_volume.get_center()[i];
		
		float width = (get_bounding_volume(pObject).max_point[i] - get_bounding_volume(pObject).min_point[i]) * 0.5f;

		if(glm::abs(delta) <= width)
		{
			straddle = 1;
			break;
		}

		if (delta > 0.0f)
			index |= (1 << i);	
	}
	if (!straddle)
	{
		if (pTree->children[index] == nullptr)
		{
			glm::vec3 offset;
			auto step = (pTree->bounding_volume.max_point - pTree->bounding_volume.min_point) * 0.25f;
			offset.x = ((index & 1) ? step.x : -step.x);
			offset.y = ((index & 2) ? step.y : -step.y);
			offset.z = ((index & 4) ? step.z : -step.z);
			pTree->children[index] = new node;
			
			auto new_center = pTree->bounding_volume.get_center() + offset;
			auto distance = (pTree->bounding_volume.max_point - pTree->bounding_volume.get_center()) / 2.0f;
			pTree->children[index]->bounding_volume.max_point = new_center + distance;
			pTree->children[index]->bounding_volume.min_point = new_center - distance;
		}
		insert_object(pObject, stopDepth - 1,pTree->children[index], reinserted);
	}
	else
	{
		pTree->objects.push_back(pObject);
		if(!reinserted)
			inserted_objects.push_back(pObject);
	}
}
