/**
* @file		transform.h
* @date 	2/11/2019
* @author	Ivan Iglesias
* @par		Login: ivan.iglesias
* @par		Course: CS350
* @par		Assignment #2
* @brief 	Implementation of transform functions
*
* Hours spent on this assignment: 20h
*
*/
#pragma once
#include "pch.h"

class Transform
{
public:
	Transform()
	{
		Scale.x = 5.0f;
		Scale.y = 5.0f;
		Scale.z = 5.0f;
		Position.x = 0.0f;
		Position.y = 0.0f;
		Position.z = -20.0f;
		Rotate.x = 0.0f;
		Rotate.y = 0.0f;
		Rotate.z = 0.0f;
	}

	glm::vec3 Position;
	glm::vec3 start_position;
	glm::vec3 Rotate;
	glm::vec3 Scale;

	bool goUp = true;
	glm::mat4 Model;

	void UpdateMatrices();

	void Reset()
	{
		Scale.x = 5.0f;
		Scale.y = 5.0f;
		Scale.z = 5.0f;
		Position = start_position;		
		Rotate.x = 0.0f;
		Rotate.y = 0.0f;
		Rotate.z = 0.0f;
	};

private:
};