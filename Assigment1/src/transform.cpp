/**
* @file		transform.cpp
* @date 	2/11/2019
* @author	Ivan Iglesias
* @par		Login: ivan.iglesias
* @par		Course: CS350
* @par		Assignment #2
* @brief 	Implementation of transform functions
*
* Hours spent on this assignment: 20h
*
*/
#include "pch.h"
#include "transform.h"
//Update transform matrices
void Transform::UpdateMatrices()
{
	glm::mat4 ModelMatrix = glm::translate(glm::mat4(1.0f), Position);

	ModelMatrix = glm::rotate(ModelMatrix, Rotate.x, glm::vec3(1.0f, 0.0f, 0.0f));
	ModelMatrix = glm::rotate(ModelMatrix, Rotate.y, glm::vec3(0.0f, 1.0f, 0.0f));
	ModelMatrix = glm::rotate(ModelMatrix, Rotate.z, glm::vec3(0.0f, 0.0f, 1.0f));
	ModelMatrix = glm::scale(ModelMatrix, Scale);
	Model = ModelMatrix;

}
