#pragma once
#include "shapes.h"

/**
 * Class encapsulating a bounding bolume hierarachy
 */
template<typename T>
class bounding_volume_hierarchy {
public:
	/**
	 *
	 */
	struct node {
		/**
		 * If leaf, the pointer to the contained objects
		 */
		std::vector<T> objects{};

		/**
		 * Bounding volume of the current node
		 */
		aabb bounding_volume{};

		/**
		 * Pointers to the children
		 */
		std::array<node*, 2> children{};
	};

private:
	node* m_root{};
public:
	~bounding_volume_hierarchy() 
	{	
		if(m_root != nullptr)
			clear_bvh(m_root);
	};
	node* build_top_down(const std::vector<T>& objects);
	void recursive_build_top_down(const std::vector<T>& objects, node * prev);
	void PartitionObjects(const aabb & big_aabb, const std::vector<T>& objects, std::vector<T>& objects_left, std::vector<T>& objects_right);
	void build_bottom_up(const std::vector<T>& objects);
	void add_object(const T& object);
	bool remove_object(const T& object, node** current = nullptr);
	node * root() { return m_root; };
	const aabb& get_bounding_volume(const aabb& lhs);
	aabb create_aabb(const std::vector<T>& objects);
	bool check_aabb(const std::vector<T>& objects);
	void nodes_merge(const std::vector<node *> & all_nodes, int & i, int & j);
	aabb compute_bounding_frow_two(const aabb & aabb1, const aabb & aabb2);
	void clear_bvh(node * root);
	void delete_tree(node * root);


};

/**
 * Prints a tree
 * You can save the content of this function, enclosed by "digraph G {" + print_tree(...) + "}"
 * 	and then call dot dot_input -Tpng  -o out.png to view the actual tree
 * @tparam T
 * @param node
 * @param node_name
 * @param stream
 * @return
 */
template<typename T>
std::ostream& print_tree(const T* node, const std::string& node_name, std::ostream& stream, bool line_breaks)
{
	// Base case
	if (!node){
		return stream;
	}

	// Print contents (in comment)
	stream << "/* ";
	stream << "[Objects: " << node->objects.size() << ", ";
	stream << node->bounding_volume << "] ";
	stream << "*/ ";
	if (line_breaks){
		stream << "\n";
	}
	// Print node name
	stream << node_name << ";";
	if (line_breaks){
		stream << "\n";
	}

	// Print children
	if (node->children[0]){
		// Create new name for child
		auto subnode_name = node_name + "0";
		// Recursively call
		print_tree(node->children[0], subnode_name, stream, line_breaks);
		// Print the link
		stream << node_name << " -> " << subnode_name << ";";
		if (line_breaks){
			stream << "\n";
		}
	}
	if (node->children[1]){
		// Create new name for child
		auto subnode_name = node_name + "1";
		// Recursively call
		print_tree(node->children[1], subnode_name, stream, line_breaks);
		// Print the link
		stream << node_name << " -> " << subnode_name << ";";
		if (line_breaks){
			stream << "\n";
		}
	}
	return stream;
}
/**
* @brief 	function to divide objects
* @param	big_aabb	main aabb
* @param	objects		all the objects to test with
* @param	objects_left		objects that will lie on the left
* @param	objects_right	objects that will lie on the right
*/
template<typename T>
void bounding_volume_hierarchy<T>::PartitionObjects(const aabb & big_aabb,const std::vector<T>& objects, std::vector<T>& objects_left, std::vector<T>& objects_right)
{
	glm::vec3 size = big_aabb.max_point - big_aabb.min_point;
	int axis = 0;
	//find the bigger axis
	if (size.x >= size.y && size.x >= size.z)
	{
		axis = 0;
	}
	else if (size.y >= size.z)
	{
		axis = 1;
	}
	else
		axis = 2;

	//compute the average
	float av = 0.0f;
	for (unsigned i = 0; i < objects.size(); i++)
	{
		av += get_bounding_volume(objects[i]).get_center()[axis];		
	}
	av /= objects.size();

	//divide the objects based on the average point
	for (unsigned i = 0; i < objects.size(); i++)
	{
		auto center = get_bounding_volume(objects[i]).get_center()[axis];
		if (av > center)
		{
			objects_left.push_back(objects[i]);
		}
		else
		{
			objects_right.push_back(objects[i]);
		}
	}
	
	return;
}
/**
* @brief 	function that makes a top down tree
* @param	objects	objects to make the tree with
* @return	return the head node of the tree
*/
template<typename T>
typename bounding_volume_hierarchy<T>::node * bounding_volume_hierarchy<T>::build_top_down(const std::vector<T>& objects)
{
	//clear bounding volume before using if not empty
	clear_bvh(m_root);
	m_root = new node;	
	
	//create the smallest aabb possible
	aabb big_aabb = create_aabb(objects);

	m_root->bounding_volume = big_aabb;
	//call recursive
	recursive_build_top_down(objects, m_root);	
	
	return m_root;
}
/**
* @brief 	recursive function that makes a top down tree
* @param	objects	objects to make the tree with
* @param	prev	previous pointer
* @return	return the head node of the tree
*/
template<typename T>
void bounding_volume_hierarchy<T>::recursive_build_top_down(const std::vector<T>& objects, node * prev)
{	

	if (objects.size() <= 1 || check_aabb(objects))
	{
		prev->objects = objects;		
	}
	else
	{
		// Based on some partitioning strategy, arrange objects into
		// two partitions: object[0..k-1], and object[k..numObjects-1]
		std::vector<T> objects_left;
		std::vector<T> objects_right;
		PartitionObjects(prev->bounding_volume, objects, objects_left, objects_right);
		// Recursively construct left and right subtree from subarrays and
		// point the left and right fields of the current node at the subtrees
		prev->children[0] = new node;
		prev->children[1] = new node;

		prev->children[0]->bounding_volume = create_aabb(objects_left);
		prev->children[1]->bounding_volume = create_aabb(objects_right);

		recursive_build_top_down(objects_left, prev->children[0]);
		recursive_build_top_down(objects_right, prev->children[1]);

	}
}
/**
* @brief 	 function that set values to merges
* @param	all_nodes	nodes to merge
* @param	i	position of first node
* @param	j	position of second node
*/
template<typename T>
void bounding_volume_hierarchy<T>::nodes_merge(const std::vector<node *> & all_nodes, int & i, int & j)
{

	glm::vec3 best_size(FLT_MAX);
	i = 0;
	j = 1;
	//iterate through the list
	for (unsigned x = 0; x < all_nodes.size(); x++)
	{
		for (unsigned y = x + 1; y < all_nodes.size(); y++)
		{		
			//find smallest bounding possible checking the sizes
			aabb new_aabb = compute_bounding_frow_two(all_nodes[x]->bounding_volume, all_nodes[y]->bounding_volume );
			auto size = new_aabb.max_point - new_aabb.min_point;
			if ((size.x * size.y * size.z) < (best_size.x * best_size.y * best_size.z))
			{
				best_size = size;
				i = x;
				j = y;
			}
		}
	}

}
/**
* @brief 	compute new aabb from two aabb
* @param	aabb1	first aabb
* @param	aabb2	second aabb
* @return	aabb result
*/
template<typename T>
aabb bounding_volume_hierarchy<T>::compute_bounding_frow_two(const aabb & aabb1, const aabb & aabb2)
{	
	aabb result_aabb{ };
	
	//make the bounding volumen that includes both aabbs
	//check all the positions
	if (aabb1.max_point.x > aabb2.max_point.x)
		result_aabb.max_point.x = aabb1.max_point.x;
	else
		result_aabb.max_point.x = aabb2.max_point.x;

	if (aabb1.max_point.y > aabb2.max_point.y)
		result_aabb.max_point.y = aabb1.max_point.y;
	else
		result_aabb.max_point.y = aabb2.max_point.y;

	if (aabb1.max_point.z > aabb2.max_point.z)
		result_aabb.max_point.z = aabb1.max_point.z;
	else
		result_aabb.max_point.z = aabb2.max_point.z;

	if (aabb1.min_point.x < aabb2.min_point.x)
		result_aabb.min_point.x = aabb1.min_point.x;
	else
		result_aabb.min_point.x = aabb2.min_point.x;

	if (aabb1.min_point.y < aabb2.min_point.y)
		result_aabb.min_point.y = aabb1.min_point.y;
	else
		result_aabb.min_point.y = aabb2.min_point.y;

	if (aabb1.min_point.z < aabb2.min_point.z)
		result_aabb.min_point.z = aabb1.min_point.z;
	else
		result_aabb.min_point.z = aabb2.min_point.z;

	return result_aabb;
}
/**
* @brief 	deletes all the nodees
* @param	root	root of the bvh
*/
template<typename T>
void bounding_volume_hierarchy<T>::clear_bvh(node * root)
{
	delete_tree(root);
	//delete root
	m_root = nullptr;
}
/**
* @brief 	deletes all the nodes
* @param	root	root of the bvh
*/
template<typename T>
void bounding_volume_hierarchy<T>::delete_tree(node * root)
{
	//if root does not exist directly return
	if (!root)
		return;
	if (!root->children[0] && !root->children[1])
	{
		delete root;
		return;
	}
	else
	{
		//call the recursive with the childs
		delete_tree(root->children[0]);
		delete_tree(root->children[1]);
		delete root;
	}
}
/**
* @brief 	builds bottom up
* @param	objects	objects to build bottom up
*/
template<typename T>
void bounding_volume_hierarchy<T>::build_bottom_up(const std::vector<T>& objects)
{
	//return;
	//allocate memory for all the nodes
	//build all the bounding volumes

	if (objects.empty())	
		return;

	//clear bounding volume before using if not empty
	clear_bvh(m_root);
	
	std::vector<node *> all_nodes{ objects.size(), nullptr };


	//set the bounding volumes to all the nodes
	for (unsigned i = 0; i < objects.size(); i++)
	{
		all_nodes[i] = new node;
		all_nodes[i]->bounding_volume = get_bounding_volume(objects[i]);
		all_nodes[i]->objects = { objects[i] };
	}
	int i, j;
	while (all_nodes.size() > 1)
	{
		//find nodes to merge
		nodes_merge(all_nodes, i, j);
		node * merge_node = new node;
		merge_node->children[0] = all_nodes[i];
		merge_node->children[1] = all_nodes[j];
		//create new volume
		merge_node->bounding_volume = compute_bounding_frow_two(all_nodes[i]->bounding_volume, all_nodes[j]->bounding_volume);
		all_nodes.erase(all_nodes.begin() + j);
		all_nodes.erase(all_nodes.begin() + i);
		all_nodes.push_back(merge_node);
	}

	//store root
	m_root = all_nodes[0];
}
/**
* @brief 	adds object to the tree
* @param	object	object to add
*/
template<typename T>
void bounding_volume_hierarchy<T>::add_object(const T & object)
{
	if (!m_root)
	{
		node * root_node = new node;
		root_node->objects = { object };
		root_node->bounding_volume = get_bounding_volume(object);
		m_root = root_node;
		return;
	}
	node ** pnode = &m_root;
	while ((*pnode)->children[0] && (*pnode)->children[1])
	{
		//compute aabb with left
		auto aabb_left = compute_bounding_frow_two((*pnode)->children[0]->bounding_volume, get_bounding_volume(object));
		//compute bounding right
		auto aabb_right = compute_bounding_frow_two((*pnode)->children[1]->bounding_volume, get_bounding_volume(object));

		//compute size
		auto size_left_vec = aabb_left.max_point - aabb_left.min_point;
		auto size_right_vec = aabb_right.max_point - aabb_right.min_point;

		auto prev_size_left_vec = (*pnode)->children[0]->bounding_volume.max_point - (*pnode)->children[0]->bounding_volume.min_point;
		auto prev_size_right_vec = (*pnode)->children[1]->bounding_volume.max_point - (*pnode)->children[1]->bounding_volume.min_point;

		auto size_left = size_left_vec.x * size_left_vec.y * size_left_vec.z;
		auto size_right = size_right_vec.x * size_right_vec.y * size_right_vec.z;

		auto prev_size_left = prev_size_left_vec.x * prev_size_left_vec.y * prev_size_left_vec.z;
		auto prev_size_right = prev_size_right_vec.x * prev_size_right_vec.y * prev_size_right_vec.z;
		//compute new bounding for parent	
		(*pnode)->bounding_volume = compute_bounding_frow_two((*pnode)->bounding_volume, get_bounding_volume(object));
		//compare sizes
		if ((size_left - prev_size_left) < (size_right - prev_size_right)) //left smaller		
			pnode = &(*pnode)->children[0];
		else
			pnode = &(*pnode)->children[1];
	}
	node * new_node_left = new node;
	node * new_node_right = new node;

	//set left child
	new_node_left->bounding_volume = (*pnode)->bounding_volume;
	new_node_left->objects = (*pnode)->objects;

	//set right child
	new_node_right->bounding_volume = get_bounding_volume(object);
	new_node_right->objects = { object };

	//set variables to null
	(*pnode)->bounding_volume = compute_bounding_frow_two(new_node_left->bounding_volume, new_node_right->bounding_volume);
	(*pnode)->objects.clear();

	//set childs
	(*pnode)->children[0] = new_node_left;
	(*pnode)->children[1] = new_node_right;

}
/**
* @brief 	removes object from the tree
* @param	object	object to remove
* @param	current	object to remove
*/
template<typename T>
bool bounding_volume_hierarchy<T>::remove_object(const T & object, node ** current)
{
	//if we receiver a null pointer
	if (!current)
	{
		//we call the function with the root 
		return remove_object(object, &m_root);
	}
	//if we found the object and it is a leaf 
	if (check_aabb({ object , (*current)->bounding_volume }))
	{
		(*current)->objects.clear();
		delete (*current);
		*current = nullptr;
		//we go back to the parent
		return true;
	}
	else if (!(*current)->children[0] && !(*current)->children[1]) //object not on this side
		return false;

	//call remove object with both of the childs
	if (remove_object(object, &(*current)->children[0]) || remove_object(object, &(*current)->children[1]))
	{
		//if true it means that a leaf has been deleted
		//check if both of my children exist

		if ((*current)->children[0] == nullptr)
		{
			//left child dead so we delete ourselfs and set our other child to us
			//(*current)->object.clear();
			//save our other children
			auto point_temp = (*current)->children[1];
			delete (*current);
			*current = point_temp;
			return true;
		}
		else if ((*current)->children[1] == nullptr)
		{
		//right child dead so we delete ourselfs and set our other child to us
		//(*current)->object.clear();
		//save our other children
			auto point_temp = (*current)->children[0];
			delete (*current);
			*current = point_temp;
			return true;
		}
		else //recopute aabb as your childrens are alife
		{
			(*current)->bounding_volume = compute_bounding_frow_two((*current)->children[0]->bounding_volume, (*current)->children[1]->bounding_volume);
			return true;
		}
	}
	return false;
}
/**
* @brief 	return the aabb
* @param	lhs	object to remove
* @return	aabb result
*/
template<typename T>
const aabb & bounding_volume_hierarchy<T>::get_bounding_volume(const aabb & lhs)
{
	return lhs;
}
/**
* @brief 	create an aabb from objects
* @param	objects	objects to create an aabb
* @return	aabb result
*/
template<typename T>
aabb bounding_volume_hierarchy<T>::create_aabb(const std::vector<T>& objects)
{
	if (objects.empty())
		return {};
	aabb result_aabb{ vec3(FLT_MAX), vec3(-FLT_MAX)};	
	for (unsigned i = 0; i < objects.size(); i++)
	{
		//computes aabb from objects
		auto temp_aabb = get_bounding_volume(objects[i]);
		if (temp_aabb.max_point.x > result_aabb.max_point.x)
			result_aabb.max_point.x = temp_aabb.max_point.x;
		if (temp_aabb.max_point.y > result_aabb.max_point.y)
			result_aabb.max_point.y = temp_aabb.max_point.y;
		if (temp_aabb.max_point.z > result_aabb.max_point.z)
			result_aabb.max_point.z = temp_aabb.max_point.z;

		if (temp_aabb.min_point.x < result_aabb.min_point.x)
			result_aabb.min_point.x = temp_aabb.min_point.x;
		if (temp_aabb.min_point.y < result_aabb.min_point.y)
			result_aabb.min_point.y = temp_aabb.min_point.y;
		if (temp_aabb.min_point.z < result_aabb.min_point.z)
			result_aabb.min_point.z = temp_aabb.min_point.z;
	}
	return result_aabb;

}
/**
* @brief 	checks if there exist and aabb
* @param	objects	objects to check if equal
* @return	if objects are equal
*/
template<typename T>
bool bounding_volume_hierarchy<T>::check_aabb(const std::vector<T>& objects)
{
	if (objects.size() < 2)
		return false;
	aabb aabb1 = get_bounding_volume(objects[0]);
	aabb aabb2 = get_bounding_volume(objects[1]);
	//check if aabbs are equal
	if (aabb1.max_point == aabb2.max_point && aabb1.min_point == aabb2.min_point)
		return true;
	else
		return false;
}

