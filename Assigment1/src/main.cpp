#include "pch.h"
#include <gtest/gtest.h>
#include <cfenv>

int main_demo(int argc, char** argv);

GTEST_API_ int main(int argc, char** argv)
{

	//Floating point exceptions
#ifdef _MSC_VER
	unsigned exceptions = 0;
	exceptions |= EM_ZERODIVIDE;
	exceptions |= EM_INVALID;
	_controlfp(~exceptions, _MCW_EM);
#else
	feenableexcept(FE_DIVBYZERO | FE_INVALID);
#endif

	if (argc > 1 && strcmp(argv[1], "demo") == 0){
		// Demo
		return main_demo(argc, argv);
	}
	else{
		// GTest
		testing::InitGoogleTest(&argc, argv);
		return RUN_ALL_TESTS();
	}
}
