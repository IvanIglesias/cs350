/**
* @file		shapes.cpp
* @date 	1/18/2019
* @author	Ivan Iglesias
* @par		Login: ivan.iglesias
* @par		Course: CS350
* @par		Assignment #1
* @brief 	Implementation of shapes
*
* Hours spent on this assignment: 10h
*
*/
#include "pch.h"
#include "geometry.h"
#include "shapes.h"
/**
* @brief 	creates a ray
* @param	start		start of the ray, a poitn
* @param	dir			vector direction of the ray 
*/
ray::ray(const vec3 &start, const vec3 &dir) :
        start(start), dir(dir) {
}
/**
* @brief 	Creates plane
* @param	triangle	to create a plane
*/
plane::plane(const triangle &triangle) {
	set(triangle);
}
/**
* @brief 	Creates plane
* @param	normal	normal for the plane
* @param	point	point that defines the plane
*/
plane::plane(const vec3 &normal, const vec3 &point) {
	set(normal, point);
}
/**
* @brief 	creates plane from triangle
* @param	triangle	triangle for the plane
*/
void plane::set(const triangle &triangle) {
	this->normal = glm::cross(triangle.points[1] - triangle.points[0],
		triangle.points[2] - triangle.points[0]);
	normal = glm::normalize(this->normal);
	this->dot_result = glm::dot(triangle.points[0], normal);
}
/**
* @brief 	sets the normal and the dot result
* @param	point	point to creates the dot result
* @param	norm	normal of the plane
*/
void plane::set(const vec3 &norm, const vec3 &point) {
	this->normal = norm;
	this->dot_result = glm::dot(point, normal);
}
/**
* @brief 	returns the point of the plane
* @return	The point
*/
vec3 plane::get_point() const {
    return dot_result * normal;
}
/**
* @brief 	constructor of the sphere
* @param	center	center of the sphere
* @param	radius	radius of the sphere
*/
sphere::sphere(const vec3 &center, float radius) :
        center(center),
        radius(radius) {

}
/**
* @brief 	constructor of the aabb
* @param	min_point	min point of the aabb
* @param	max_point	max point of the aabb
*/
aabb::aabb(const vec3 &min_point, const vec3 &max_point) :
        min_point(min_point),
        max_point(max_point) {
}
/**
* @brief 	returns the center of the aabb
* @return	the center vector
*/
vec3 aabb::get_center() const {
    return (min_point + max_point) * 0.5f;
}
float aabb::compute_surface() const
{
	auto lenght = max_point - min_point;
	return ((lenght.x * lenght.y) + (lenght.x * lenght.z) + (lenght.z * lenght.y)) * 2.0f;
}
/**
* @brief 	creates the frustrum
* @param	planes		Set of planes
*/
frustum::frustum(std::array<plane, 6> planes) :
        planes(planes) {
}
/**
* @brief 	creates the frustrum from the vp matrix
* @param	vp		viewport matrix
*/
frustum::frustum(const mat4 &vp) {

	auto inverse = glm::inverse(vp);


	vec4 points_ndc[8]{
		inverse * vec4(-1,1,-1,1),
		inverse * vec4(-1,1,1,1),
		inverse * vec4(-1,-1,-1,1),
		inverse * vec4(-1,-1,1,1),
		inverse * vec4(1,1,-1,1),
		inverse * vec4(1,1,1,1),
		inverse * vec4(1,-1,-1,1),
		inverse * vec4(1,-1,1,1)
	};

	for (unsigned i = 0; i < 8; i++)
	{		
		points_ndc[i] /= points_ndc[i].w;
	}


	this->planes[0].set(triangle{ glm::vec3(points_ndc[0]) , glm::vec3(points_ndc[3]), glm::vec3(points_ndc[1]) });
	this->planes[1].set(triangle{ glm::vec3(points_ndc[4]) , glm::vec3(points_ndc[7]), glm::vec3(points_ndc[6]) });
	this->planes[2].set(triangle{ glm::vec3(points_ndc[0]) , glm::vec3(points_ndc[1]), glm::vec3(points_ndc[5]) });
	this->planes[3].set(triangle{ glm::vec3(points_ndc[2]) , glm::vec3(points_ndc[7]), glm::vec3(points_ndc[3]) });
	this->planes[4].set(triangle{ glm::vec3(points_ndc[0]) , glm::vec3(points_ndc[6]), glm::vec3(points_ndc[2]) });
	this->planes[5].set(triangle{ glm::vec3(points_ndc[3]) , glm::vec3(points_ndc[7]), glm::vec3(points_ndc[1]) });
}
/**
* @brief 	returns the points of the plane
* @return	The set of points of the frustrum
*/
std::array<vec3, 8> frustum::get_points() const {
 
	std::array<vec3, 8> result;

	intersection_plane_plane_plane(planes[0], planes[2], planes[4], &result[0]);
	intersection_plane_plane_plane(planes[0], planes[2], planes[5], &result[1]);
	intersection_plane_plane_plane(planes[0], planes[3], planes[4], &result[2]);
	intersection_plane_plane_plane(planes[0], planes[3], planes[5], &result[3]);
	intersection_plane_plane_plane(planes[1], planes[2], planes[4], &result[4]);
	intersection_plane_plane_plane(planes[1], planes[2], planes[5], &result[5]);
	intersection_plane_plane_plane(planes[1], planes[3], planes[4], &result[6]);
	intersection_plane_plane_plane(planes[1], planes[3], planes[5], &result[7]);
	
	return result;
}
