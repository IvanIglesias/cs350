/**
* @file		windows.h
* @date 	1/18/2019
* @author	Ivan Iglesias
* @par		Login: ivan.iglesias
* @par		Course: CS350
* @par		Assignment #1
* @brief 	Include of windows.h
*
* Hours spent on this assignment: 10h
*
*/
#pragma once

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif