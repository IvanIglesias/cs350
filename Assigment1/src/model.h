/**
* @file		model.h
* @date 	2/11/2019
* @author	Ivan Iglesias
* @par		Login: ivan.iglesias
* @par		Course: CS350
* @par		Assignment #2
* @brief 	Implementation of the model object
*
* Hours spent on this assignment: 20h
*
*/
#pragma once
#include "pch.h"
#include "mesh.h"
#include "transform.h"
#include "geometry.h"


class Model
{
public:
	Model() {};
	Model(unsigned index_for_mesh) : mesh_index(index_for_mesh) {};
	
	Transform TransformData;
	vec4 color_aabb{0.2, 0.1, 0.1, 0.75};
	unsigned mesh_index;
	aabb transform_aabb;
	aabb bounding_aabb;
	sphere bounding_shpere;

private:

};
