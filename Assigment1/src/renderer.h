/**
* @file		renderer.h
* @date 	1/18/2019
* @author	Ivan Iglesias
* @par		Login: ivan.iglesias
* @par		Course: CS350
* @par		Assignment #1
* @brief 	Implementation of renderer functions
*
* Hours spent on this assignment: 10h
*
*/
#pragma once
#include "shapes.h"
#include "mesh.h"
#include "camera.h"
#include "model.h"
#include "octree.h"
#include "kdtree.h"

class renderer {
private:

public:
	bool entry_point();

	// Debug
	void debug_render_sphere(const sphere& sphere, const vec4& color);
	void debug_render_aabb(const aabb& aabb, const vec4& color);
	void debug_render_point(const vec3& point, const vec4& color);
	void debug_render_segment(const vec3& start, const vec3& end, const vec4& color);
	void debug_render_triangle(const triangle& triangle, const vec4& color);
	void debug_render_plane(const plane& plane, const vec4& color);
	void debug_render_frustum(const frustum& frustum, const vec4& color);

	std::vector<Mesh> objects_mesh;
	std::vector<Model> bounding_models;
	std::vector<Model *> objects_render;

	frustum cam_frustrum;
	bool freeze_frustum{ false };

	int bounding_type = 0;
	int aabb_type = 1;
	int sphere_type = 0;
	void load_all_meshes();
	void processInput(GLFWwindow *window);
	Camera cam;
	void generate_objects();
	void add_object();
	int object_num{-1};
	int tot_obj_num{ 1 };
	bool rotate{ false };
	bool imgui_hovered{ false };
	//int bvh_method = 0;
	const glm::vec4 default_aabb_color{ 0.2, 0.1, 0.1, 1.0 };
	int selecting = -1;
	int prev = -1;
	//kdtree
	bool create_kdtree{ true };
	kdtree tree_gourd;
	int tree_level{ 0 };
	int max_depth{ 7 };

	//octtree
	bool create_octree{ true };
	bool insert_octree_last{ false };

	//octree<Model> my_octree;

	double mouse_xpos, mouse_ypos;
	vec3 mouse_pos;

};

int main_demo(int argc, char** argv);
