/**
* @file		mesh.h
* @date 	2/11/2019
* @author	Ivan Iglesias
* @par		Login: ivan.iglesias
* @par		Course: CS350
* @par		Assignment #2
* @brief 	Implementation of the mesh
*
* Hours spent on this assignment: 20h
*
*/
#pragma once
#include <vector>
#include "obj_loader.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
class Mesh
{
public:	
	Mesh(std::string file_name);

	std::vector < vec3 > out_vertices;
	std::vector < short unsigned > out_index_vert;
	std::vector < vec3 > out_normals;

	void SetBuffers();

	GLuint vao;
	GLuint positionBufferObject;
	GLuint indexBufferObject;

private:

};