#pragma once
/**
* @file		gjk.h
* @date 	4/24/2019
* @author	Ivan Iglesias
* @par		Login: ivan.iglesias
* @par		Course: CS350
* @par		Assignment #6
* @brief 	Implementation of the gjk algorithm functions
*
*/
glm::vec2 get_farthest_point_in_direction(const std::vector<glm::vec2> shape_a, const glm::vec2 direction)
{
	int farthestIndex = 0;

	auto farthestDistance = glm::dot(shape_a[0], direction);

	for (unsigned i = 0; i < shape_a.size(); ++i)
	{
		auto tmp = dot(shape_a[i], direction);		
		if (tmp > farthestDistance) 
		{
			farthestDistance = tmp;
			farthestIndex = i;
		}
	}
	return shape_a[farthestIndex];
}
/**
* @brief 	function to obtain the fartherst
* @param	direction direction to optain the farthest point from
* @param	shape_a shape from where to obtain the farthest point
* @return	farthes point in the direction
*/
glm::vec3 get_farthest_point_in_direction(const std::vector<glm::vec3> shape_a, const glm::vec3 direction)
{
	int farthestIndex = 0;

	auto farthestDistance = glm::dot(shape_a[0], direction);

	for (unsigned i = 0; i < shape_a.size(); ++i)
	{
		auto tmp = dot(shape_a[i], direction);
		if (tmp > farthestDistance)
		{
			farthestDistance = tmp;
			farthestIndex = i;
		}
	}
	return shape_a[farthestIndex];
}
/**
* @brief 	function compute the mikowski difference
* @param	shape_a shape from where to compute the support point
* @param	shape_b shape from where to compute the support point
* @param	direction direction to optain the farthest points 
* @return	mikowski difference point
*/
glm::vec2 support_point(const std::vector<glm::vec2> shape_a, const std::vector<glm::vec2> shape_b, const glm::vec2 direction)
{
	//get the points on the edge of the shapes in opposite directions
	glm::vec2 point1 = get_farthest_point_in_direction(shape_a, direction);
	glm::vec2 point2 = get_farthest_point_in_direction(shape_b, -direction);
	//compute minkowski difference and return the point
	return point1 - point2;
}
/**
* @brief 	function compute the mikowski difference
* @param	shape_a shape from where to compute the support point
* @param	shape_b shape from where to compute the support point
* @param	direction direction to optain the farthest points
* @return	mikowski difference point
*/
glm::vec3 support_point(std::vector<glm::vec3> shape_a, std::vector<glm::vec3> shape_b, glm::vec3 direction)
{
	//get the points on the edge of the shapes in opposite directions
	glm::vec3 point1 = get_farthest_point_in_direction(shape_a, direction);
	glm::vec3 point2 = get_farthest_point_in_direction(shape_b, -direction);
	//compute minkowski difference and return the point
	return point1 - point2;
}
/**
* @brief 	function compute the triple product in 2D
* @param	a edge compute the triple product
* @param	b edge compute the triple product
* @param	c edge compute the triple product
* @return	the result of the triple product
*/
glm::vec2 tripleProduct(const glm::vec2& a, const glm::vec2& b, const glm::vec2& c)
{
	return (b * glm::dot(a, c)) - (a * glm::dot(b, c));
}
/**
* @brief 	function to compute the magatide squared
* @param	v vector to compute the magnitude square
* @return	the magnitude squared
*/
float magnitudeSquared(const glm::vec2& v)
{
	return v.x * v.x + v.y * v.y;
}
/**
* @brief 	function to swap the vector positions
* @param	v vector to swap
* @return	the vectro swaped
*/
glm::vec2 left(const glm::vec2& v)
{
	return glm::vec2(v.y, -v.x);
}
/**
* @brief 	function to check and updated the simplex in 2D
* @param	simplex vector that contains all the points of our simplex
* @param	direction to update with each new simplex
* @return	if colliding or not
*/
bool checkSimplex(std::vector<glm::vec2> & simplex, glm::vec2 & direction) {
	// get the last point
	glm::vec2 a = simplex.back();

	// get the las point negated
	glm::vec2 ao = -a;

	// check to see what type of simplex we have
	//in this case we have a triangle
	if (simplex.size() == 3)
	{
		//create the triangle
		glm::vec2 b = simplex[1];
		glm::vec2 c = simplex[0];

		// get the edges
		glm::vec2 ab = b - a;
		glm::vec2 ac = c - a;

		// get the edge normals
		glm::vec2 abPerp = tripleProduct(ac, ab, ab);
		glm::vec2 acPerp = tripleProduct(ab, ac, ac);

		// see where the origin is at
		float ac_loc = glm::dot(acPerp, ao);
		
		if (ac_loc > 0.0) {
			//not a valid point, erase it and set the direction to the perpendicular
			simplex.erase(simplex.begin() + 1);		
			direction = acPerp;
		}
		else {
			float abLocation = dot(abPerp, ao);

			// In this case the origin will lie on the left side so we have a collision
			if (abLocation < 0.0) {			
				return true;
			}
			else {
				//Origin lies between A and B, not a valid point, erase it and change direction
				simplex.erase(simplex.begin());				
				direction = abPerp;
			}
		}
	}
	else {
		// get the b point
		glm::vec2 b = simplex[0];
		glm::vec2 ab = b - a;

		//set the direction to the perdendicular with the triple product
		direction = tripleProduct(ab, ao, ab);

		//in case the origin lying on the segment we swap the direction to the left
		if (magnitudeSquared(direction) <= cEpsilon)	
			direction = left(ab);	
	}
	return false;
}
/**
* @brief 	function to check collision in 2D with gjk
* @param	shape_a colliding shape
* @param	shape_b colliding shape
* @return	if colliding or not
*/
bool gjk_2d(std::vector<glm::vec2> shape_a, std::vector<glm::vec2> shape_b)
{
	//1. Initialize the simplex set Q with up to d + 1
	//	points from C(in d dimensions)

	std::vector<glm::vec2> simplex;

	//get the first direction
	glm::vec2 center_a{0,0};
	glm::vec2 center_b{0,0};

	//compute the medium point of each shape
	for (unsigned i = 0; i < shape_a.size(); i++)
		center_a += shape_a[i];
	center_a /= shape_a.size();
	for (unsigned i = 0; i < shape_b.size(); i++)
		center_b += shape_b[i];
	center_b /= shape_b.size();

	auto init_direction = center_b - center_a;
	//add the first point

	simplex.push_back(support_point(shape_a, shape_b, init_direction));

	//past the origin so the shapes do not intersect
	if (glm::dot(simplex[0], init_direction) < 0.0) 
		return false;
	//swap the direction
	auto direction = -init_direction;

	while (true)
	{
		//add another point to the simplex
		simplex.push_back(support_point(shape_a, shape_b, direction));

		//check that the last point was past the origin
		glm::vec2 last = simplex.back();
		if (glm::dot(last, direction) <= 0.0) //not past the origin so the shapes do not intersect
			return false;
		else
		{	//test if the simplex contains the origin
			if (checkSimplex(simplex, direction))
				return true;
		}
	}	
	return false;
}
/**
* @brief 	function to check and updated the simplex in 3D
* @param	simplex vector that contains all the points of our simplex
* @param	direction to update with each new simplex
* @return	if colliding or not
*/
bool checkSimplex(std::vector<glm::vec3> & simplex, glm::vec3 & direction) 
{
	switch (simplex.size())
	{
	case 2:
	{
		//line formed by the first two vertices
		auto ab = simplex[1] - simplex[0];
		//line formed by the first vertex to the origin
		auto a0 = -simplex[0];
		//compute the triple cross product to obtain a perpendicular direction
		auto perp = glm::cross(ab, a0);
		direction = glm::cross(perp, direction);
		break;
	}
	case 3:
	{
		//line formed by the first two vertices
		auto ac = simplex[2] - simplex[0];
		//line formed by the first vertex to the origin
		auto ab = simplex[1] - simplex[0];
		//compute the triple cross product to obtain a perpendicular direction
		direction = glm::cross(ac, ab);
		//check direction to the center
		auto a0 = -simplex[0];
		if (glm::dot(direction, a0) < 0)
			direction = -direction;
		break;
	}
	case 4:
	{
		//compute the edges
		auto da = simplex[3] - simplex[0];
		auto db = simplex[3] - simplex[1];
		auto dc = simplex[3] - simplex[2];
		auto d0 = -simplex[3];

		//check triangles
		auto norm_abd = glm::cross(da, db);
		auto norm_bcd = glm::cross(db, dc);
		auto norm_cad = glm::cross(dc, da);
		//Check where the point lies and if not valid, erase it
		if (glm::dot(norm_abd, d0) > 0)
		{
			simplex.erase(simplex.begin() + 2);
			direction = norm_abd;
		}
		else if (glm::dot(norm_bcd, d0) > 0)
		{
			simplex.erase(simplex.begin());
			direction = norm_bcd;
		}
		else if (glm::dot(norm_cad, d0) > 0)
		{
			simplex.erase(simplex.begin() + 1);
			direction = norm_cad;
		}
		else
		{
			//intersection found
			return true;
		}
		break;
	}
	}

	return false;	
}
/**
* @brief 	function to check collision in 3D with gjk
* @param	shape_a colliding shape
* @param	shape_b colliding shape
* @return	if colliding or not
*/
bool gjk_3d(std::vector<glm::vec3> shape_a, std::vector<glm::vec3> shape_b)
{

	//1. Initialize the simplex set Q with up to d + 1
	//	points from C(in d dimensions)

	std::vector<glm::vec3> simplex;

	//get the first direction
	glm::vec3 center_a{ 0,0,0 };
	glm::vec3 center_b{ 0,0,0 };

	for (unsigned i = 0; i < shape_a.size(); i++)
		center_a += shape_a[i];
	center_a /= shape_a.size();
	for (unsigned i = 0; i < shape_b.size(); i++)
		center_b += shape_b[i];
	center_b /= shape_b.size();

	auto init_direction = center_b - center_a;
	//add the first point 

	simplex.push_back(support_point(shape_a, shape_b, init_direction));
	//past the origing s
	if (glm::dot(simplex[0], init_direction) < 0.0)
		return false;

	auto direction = -init_direction;

	while (true)
	{
		//add another point to the simplex
		simplex.push_back(support_point(shape_a, shape_b, direction));

		//check that the last point was past the origin
		glm::vec3 last = simplex.back();
		auto dot = glm::dot(last, direction);
		if (dot < 0.0) //not past the origin so the shapes do not intersect
			return false;
		else
		{	//test if the simplex contains the origin
			if (checkSimplex(simplex, direction))
				return true;
		}
	}
	return false;
}
