/**
* @file		kdtree.h
* @date 	4/3/2019
* @author	Ivan Iglesias
* @par		Login: ivan.iglesias
* @par		Course: CS350
* @par		Assignment #4
* @brief 	Implementation of kdtree functions
*
* Hours spent on this assignment: 15h
*
*/
#pragma once
#include "geometry.h"
#include "mesh.h"
#include <set>

class kdtree
{
public:

	struct KDNode
	{
		KDNode *child[2] = { nullptr };
		int   plane_axis = 0;
		float plane_position = 0.0f;
		std::vector<triangle> triangles_list;
		aabb node_aabb;
	};

	struct plane_limits
	{
		std::array<int, 2> limits_x;
		std::array<int, 2> limits_y;
		std::array<int, 2> limits_z;
	};
	KDNode * m_root{ nullptr };
	void build(std::vector<triangle> triangles,  unsigned depth);
	void compute_vectors(std::vector<triangle> & triangles);
	kdtree::KDNode * build_recursive(std::vector<triangle>& triangles, aabb node_aabb, int depth, plane_limits limits_planes);
	int max_depth{ 0 };
	std::vector<KDNode*> get_breadth_first_sorted();
	bool split_plane(aabb node_aabb, std::vector<triangle> triangles, unsigned axis, float & split_pos, plane_limits limits_planes, int & index_pos);
	float heuristics(unsigned triangles_left_num, unsigned triangles_right_num, aabb aabb_left, aabb aabb_right, aabb aabb_big);
	
	std::vector<float> planes_x;
	std::vector<float> planes_y;
	std::vector<float> planes_z;

	KDNode * get_root() { return m_root; }

	void clear_tree();
	~kdtree() { clear_tree(); }
private:
	void clear_tree_recursive(KDNode *);


};

// To be implemented by the student
std::vector<triangle> load_triangles_from_obj(const char* filename);

