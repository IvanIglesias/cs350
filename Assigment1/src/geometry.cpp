/**
* @file		geometry.cpp
* @date 	1/18/2019
* @author	Ivan Iglesias
* @par		Login: ivan.iglesias
* @par		Course: CS350
* @par		Assignment #1
* @brief 	Implementation of geometry functions
*
* Hours spent on this assignment: 10h
*
*/
#include "pch.h"
#include "geometry.h"
/**
* @brief 	function to check intersection_point_plane
* @param	point		point to check intersection
* @param	plane		plane to check intersection
* @param	epsilon		epsilon to check intersection
* @return	intersection_type 
*/
intersection_type intersection_point_plane(const vec3 &point, const plane &plane, float epsilon) {
	float t = (glm::dot(plane.normal, point) - plane.dot_result) / glm::dot(plane.normal, plane.normal);

	if (t > epsilon)
		return intersection_type::INSIDE;
	if (t < -epsilon)
		return intersection_type::OUTSIDE;

	return intersection_type::OVERLAPS;

}
/**
* @brief 	function to check intersection_point_sphere
* @param	point		point to check intersection
* @param	sphere		sphere to check intersection
* @return	intersection_type
*/
intersection_type intersection_point_sphere(const vec3 &point, const sphere &sphere) {

	auto vec_center = sphere.center - point;
	auto dist = glm::length(vec_center);
	
	if (dist > sphere.radius)
		return intersection_type::OUTSIDE;	
	else
		return intersection_type::INSIDE;
}
/**
* @brief 	function to check intersection_point_aabb
* @param	point		point to check intersection
* @param	aabb		aabb to check intersection
* @return	intersection_type
*/
intersection_type intersection_point_aabb(const vec3 &point, const aabb &aabb) {

	if(point.x > aabb.max_point.x || point.x < aabb.min_point.x ||
		point.y > aabb.max_point.y || point.y < aabb.min_point.y ||
		point.z > aabb.max_point.z || point.z < aabb.min_point.z)
		 return intersection_type::OUTSIDE;
	else
		return intersection_type::INSIDE;

}
/**
* @brief 	function to check intersection_ray_plane
* @param	ray		ray to check intersection
* @param	plane		plane to check intersection
* @return	time of intersection
*/
float intersection_ray_plane(const ray &ray, const plane &plane) {

	//find if ray and plane are parallel
	if (ray.dir == glm::zero<vec3>())
		return -1;
	auto denom = glm::dot(plane.normal, ray.dir);
	float t;
	if (glm::abs(denom) < cEpsilon)
		return -1;

	t = (plane.dot_result - glm::dot(plane.normal, ray.start)) / denom;
	
	if (t < 0)
		return -1;
	else
		return t;
	

}
/**
* @brief 	function to check intersection_ray_sphere
* @param	ray		ray to check intersection
* @param	sphere		sphere to check intersection
* @return	time of intersection
*/
float intersection_ray_sphere(const ray &ray, const sphere &sphere) {




	auto vec = ray.start - sphere.center;

	if (ray.dir == glm::zero<vec3>())
		return -1;

	float b = glm::dot(vec, glm::normalize(ray.dir));

	float c = glm::dot(vec, vec) - sphere.radius * sphere.radius;
	//Exit if origin is outside and not pointing
	if (c > 0.0f && b > 0.0f)
		return -1;
	float discr = b*b - c;

	//discriminant corresponds to ray missing sphere
	if (discr < 0.0f)
		return -1;

	float t = -b - glm::sqrt(discr);	
	//If t is negative ray starte inside
	if (t < 0.0f)
		t = 0.0f;

	return t;
}
/**
* @brief 	function to check intersection_ray_aabb
* @param	ray		ray to check intersection
* @param	aabb		aabb to check intersection
* @return	time of intersection
*/
float intersection_ray_aabb(const ray &ray, const aabb &aabb) {
    
	float tmin = 0.0f;
	float tmax = FLT_MAX;

	if (ray.dir == glm::zero<vec3>())
		return -1;

	for(int i = 0; i < 3; i++)
	{
		if (glm::abs(ray.dir[i]) < cEpsilon)
		{
			if (ray.start[i] < aabb.min_point[i] || ray.start[i] > aabb.max_point[i]) return -1;
		}
		else
		{
			
			float odd = 1.0f / ray.dir[i];
			float t1 = (aabb.min_point[i] - ray.start[i]) * odd;
			float t2 = (aabb.max_point[i] - ray.start[i]) * odd;

			if (t1 > t2)
			{
				float tmp = t1;
				t1 = t2;
				t2 = tmp;			
			}
			if (t1 > tmin)
				tmin = t1;
			if (t2 < tmax)
				tmax = t2;
			if (tmin > tmax)
				return -1;

		}
	}
	
	return tmin;	
}
/**
* @brief 	function to check intersection_ray_triangle
* @param	ray		ray to check intersection
* @param	triangle		triangle to check intersection
* @return	time of intersection
*/
float intersection_ray_triangle(const ray &ray, const triangle &triangle) {

	plane tri_plane;
	tri_plane.set(triangle);
	//find if ray and plane are parallel
	if (ray.dir == glm::zero<vec3>())
		return -1;

	auto denom = glm::dot(tri_plane.normal, ray.dir);

	if (denom == 0.0f)
		return -1;

	float t;

	if (glm::abs(denom) < cEpsilon)
		return -1;

	t = (tri_plane.dot_result - glm::dot(tri_plane.normal, ray.start)) / denom;

	vec3 point = ray.start + t * ray.dir;

	vec3 result;
	auto ret = get_barycentric_coordinates(triangle, point, &result);\
		
	if (!ret)
		return -1;
	else
		return t;
}
/**
* @brief 	function to check intersection_plane_triangle
* @param	plane		plane to check intersection
* @param	triangle	triangle to check intersection
* @param	epsilon		epsilon to check intersection
* @return	intersection_type
*/
intersection_type intersection_plane_triangle(const plane &plane, const triangle &triangle, float epsilon) 
{
	unsigned in = 0;
	unsigned out = 0;
	unsigned overlap = 0;

	for (unsigned i = 0; i < 3; i++)
	{
		switch (intersection_point_plane(triangle.points[i], plane, epsilon))
		{		
		case intersection_type::INSIDE:
			in++;
			break;
		case intersection_type::OVERLAPS:
			overlap++;
			break;
		case intersection_type::OUTSIDE:
			out++;
			break;
		default:
			break;
		}		
	}
	if (overlap == 3)
		return intersection_type::COPLANAR;
	else if ((in + overlap) == 3)
		return  intersection_type::INSIDE;
	else if ((out + overlap) == 3)
		return  intersection_type::OUTSIDE;
	else
		return intersection_type::OVERLAPS;	
}
/**
* @brief 	function to check intersection_plane_sphere
* @param	plane		plane to check intersection
* @param	sphere	sphere to check intersection
* @return	intersection_type
*/
intersection_type intersection_plane_sphere(const plane &plane, const sphere &sphere) {

	float dist = glm::dot(sphere.center, plane.normal) - plane.dot_result;

	if (dist < -sphere.radius)
		return intersection_type::OUTSIDE;
	else if (glm::abs(dist) <= sphere.radius)
		return intersection_type::OVERLAPS;
	else
		return intersection_type::INSIDE;
	
}
/**
* @brief 	function to check intersection_plane_aabb
* @param	plane		plane to check intersection
* @param	aabb	aabb to check intersection
* @return	intersection_type
*/
intersection_type intersection_plane_aabb(const plane &plane, const aabb &aabb) {

	
	vec3 c = (aabb.max_point + aabb.min_point) * 0.5f;
	vec3 e = aabb.max_point - c;

	float r = e.x * glm::abs(plane.normal[0]) + e.y * glm::abs(plane.normal[1]) + e.z * glm::abs(plane.normal[2]);
	float s = glm::dot(plane.normal, c) - plane.dot_result;

	if (s < -r)
		return intersection_type::OUTSIDE;
	else if (glm::abs(s) <= r)
		return intersection_type::OVERLAPS;
	else
		return intersection_type::INSIDE;


}
/**
* @brief 	function to check intersection_plane_plane_plane
* @param	plane_a		plane to check intersection
* @param	plane_b		plane to check intersection
* @param	plane_c		plane to check intersection
* @param	intersect	intersect to return intersection point
* @return	if intersection happens
*/
bool intersection_plane_plane_plane(const plane &plane_a, const plane &plane_b, const plane &plane_c, vec3 *intersect) {

	vec3 m1 = vec3(plane_a.normal.x, plane_b.normal.x, plane_c.normal.x);
	vec3 m2 = vec3(plane_a.normal.y, plane_b.normal.y, plane_c.normal.y);
	vec3 m3 = vec3(plane_a.normal.z, plane_b.normal.z, plane_c.normal.z);

	vec3 u = glm::cross(m2, m3);
	
	float denom = glm::dot(m1, u);
	if (glm::abs(denom) < cEpsilon)
		return false;
	vec3 d(plane_a.dot_result, plane_b.dot_result, plane_c.dot_result);
	
	vec3 v = glm::cross(m1, d);

	float ood = 1.0f / denom;
	
	intersect->x = glm::dot(d, u) * ood;
	intersect->y = glm::dot(m3, v) * ood;
	intersect->z = -glm::dot(m2, v) * ood;

	return true;
  
}
/**
* @brief 	function to check intersection_frustum_triangle
* @param	frustum		frustum to check intersection
* @param	triangle	triangle to check intersection
* @return	intersection_type
*/
intersection_type intersection_frustum_triangle(const frustum &frustum, const triangle &triangle) {
	
	unsigned in = 0;
	unsigned out = 0;
	unsigned overlap = 0;

	for (unsigned i = 0; i < 6; i++)
	{
		switch (intersection_plane_triangle(frustum.planes[i],triangle))
		{
		case intersection_type::INSIDE:
			in++;
			break;
		case intersection_type::OVERLAPS:
			overlap++;
			break;
		case intersection_type::OUTSIDE:
			out++;
			break;
		default:
			break;
		}
	}
	if (out)
		return intersection_type::OUTSIDE;
	else if (in == 6)
		return  intersection_type::INSIDE;

    return intersection_type::OVERLAPS;
}
/**
* @brief 	function to check intersection_frustum_triangle
* @param	frustum		frustum to check intersection
* @param	sphere	sphere to check intersection
* @return	intersection_type
*/
intersection_type intersection_frustum_sphere(const frustum &frustum, const sphere &sphere) {

	unsigned in = 0;
	unsigned out = 0;
	unsigned overlap = 0;

	for (unsigned i = 0; i < 6; i++)
	{
		switch (intersection_plane_sphere(frustum.planes[i], sphere))
		{
		case intersection_type::INSIDE:
			in++;
			break;
		case intersection_type::OVERLAPS:
			overlap++;
			break;
		case intersection_type::OUTSIDE:
			out++;
			break;
		default:
			break;
		}
	}
	if (out)
		return intersection_type::OUTSIDE;
	else if (in == 6)
		return  intersection_type::INSIDE;

	return intersection_type::OVERLAPS;
}
/**
* @brief 	function to check intersection_frustum_aabb
* @param	frustum		frustum to check intersection
* @param	aabb	aabb to check intersection
* @return	intersection_type
*/
intersection_type intersection_frustum_aabb(const frustum &frustum, const aabb &aabb) {
	unsigned in = 0;
	unsigned out = 0;
	unsigned overlap = 0;

	for (unsigned i = 0; i < 6; i++)
	{
		switch (intersection_plane_aabb(frustum.planes[i], aabb))
		{
		case intersection_type::INSIDE:
			in++;
			break;
		case intersection_type::OVERLAPS:
			overlap++;
			break;
		case intersection_type::OUTSIDE:
			out++;
			break;
		default:
			break;
		}
	}
	if (out)
		return intersection_type::OUTSIDE;
	else if (in == 6)
		return  intersection_type::INSIDE;

	return intersection_type::OVERLAPS;
}
/**
* @brief 	function to check intersection_sphere_sphere
* @param	sphere_a	sphere_a to check intersection
* @param	sphere_b	sphere_b to check intersection
* @return	if intersection happened
*/
bool intersection_sphere_sphere(const sphere &sphere_a, const sphere &sphere_b)
{
	vec3 d = sphere_a.center - sphere_b.center; 

	float dist = glm::dot(d, d);

	float radiusSum = sphere_a.radius + sphere_b.radius;

	if (dist <= radiusSum * radiusSum)
		return true;
	else
		return false;

}
/**
* @brief 	function to check intersection_aabb_aabb
* @param	aabb_a	aabb_a to check intersection
* @param	aabb_b	aabb_b to check intersection
* @return	if intersection happened
*/
bool intersection_aabb_aabb(const aabb &aabb_a, const aabb &aabb_b) {

	//Separated axis
	if (aabb_a.max_point[0] < aabb_b.min_point[0] || aabb_a.min_point[0] > aabb_b.max_point[0])
		return false;
	if (aabb_a.max_point[1] < aabb_b.min_point[1] || aabb_a.min_point[1] > aabb_b.max_point[1])
		return false;
	if (aabb_a.max_point[2] < aabb_b.min_point[2] || aabb_a.min_point[2] > aabb_b.max_point[2]) 
		return false;
	// Overlapping on all axes intersection happens
	return true;    
}
/**
* @brief 	function to return the projected_point_plane
* @param	point	point to check intersection
* @param	plane	plane to check intersection
* @return	returns the projection of the point
*/
vec3 project_point_plane(const vec3 &point, const plane &plane) {

	auto vec_result = point - glm::dot((point - plane.get_point()),
		(plane.normal / glm::dot(plane.normal,plane.normal))) * plane.normal;
    return vec_result;
}
/**
* @brief 	function to check get_barycentric_coordinates
* @param	segment_a	segment_a to get barycentric
* @param	segment_b	segment_b to get barycentric
* @param	point	point to compute barycentrix
* @param	result	result to return the result
* @return	if posible to get the barycentric
*/
bool get_barycentric_coordinates(const vec3 &segment_a, const vec3 &segment_b, const vec3 &point, vec2 *result) {

	//Compute the dot products
	auto x = glm::dot(segment_a, segment_b);
	auto y = glm::dot(point, segment_b);
	auto z = glm::dot(segment_b, segment_b);

	auto div = z - x;
	if (div == 0)
		return false;
	auto t = (y - x) / div;
	auto s = 1 - t;

	if (t > 1 || t < 0 || s > 1 || s < 0)
		return false;

	else
	{
		result->x = s;
		result->y = t;
		return true;
	}	
}
/**
* @brief 	function to check get_barycentric_coordinates of triangle
* @param	triangle triangle to get barycentric
* @param	point	point to get barycentric
* @param	result	result to return the result
* @return	if posible to get the barycentric
*/
bool get_barycentric_coordinates(const triangle &triangle, const vec3 &point, vec3 *result) {

	auto v0 = triangle.points[1] - triangle.points[0];
	auto v1 = triangle.points[2] - triangle.points[0];
	auto v2 = point - triangle.points[0];

	auto d00 = glm::dot(v0, v0);
	auto d01 = glm::dot(v0, v1);
	auto d11 = glm::dot(v1, v1);
	auto d20 = glm::dot(v2, v0);
	auto d21 = glm::dot(v2, v1);

	auto denom = d00 * d11 - d01 * d01;

	if (denom == 0)
		return false;
	auto v = (d11 * d20 - d01 * d21) / denom;
	auto w = (d00 * d21 - d01 * d20) / denom;
	auto u = 1.0f - v - w;

	if (v > 1 || v < 0 || w > 1 || w < 0 || u > 1 || u < 0)
		return false;
	else
	{
		result->x = u;
		result->y = v;
		result->z = w;
		return true;

	}

}
/**
* @brief 	function to create bounding volume for aabb from points
* @param	points points to create the aabb
* @return	the resulting aabb
*/
aabb aabb_from_points(const std::vector<vec3>& points)
{

	if (!points.size())
		return aabb{};
	aabb result_aabb;
	result_aabb.max_point.x = points[0].x;
	result_aabb.max_point.y = points[0].y;
	result_aabb.max_point.z = points[0].z;

	result_aabb.min_point.x = points[0].x;
	result_aabb.min_point.y = points[0].y;
	result_aabb.min_point.z = points[0].z;
	//Find maximum point
	for (unsigned i = 1; i < points.size(); i++)
	{
		if (points[i].x >  result_aabb.max_point.x)
			result_aabb.max_point.x = points[i].x;
		if (points[i].y >  result_aabb.max_point.y)
			result_aabb.max_point.y = points[i].y;
		if (points[i].z >  result_aabb.max_point.z)
			result_aabb.max_point.z = points[i].z;

		if (points[i].x < result_aabb.min_point.x)
			result_aabb.min_point.x = points[i].x;
		if (points[i].y < result_aabb.min_point.y)
			result_aabb.min_point.y = points[i].y;
		if (points[i].z < result_aabb.min_point.z)
			result_aabb.min_point.z = points[i].z;
	}	
	return result_aabb;
}
aabb aabb_from_triangles(const std::vector<triangle>& triangles)
{
	if (!triangles.size())
		return aabb{};

	std::vector<vec3> points;
	for (unsigned i = 0; i < triangles.size(); i++)
	{
		points.push_back(triangles[i].points[0]);
		points.push_back(triangles[i].points[1]);
		points.push_back(triangles[i].points[2]);

	}
	
	return aabb_from_points(points);
}
/**
* @brief 	function to create bounding volume for aabb from matrix
* @param	aabb_1 aabb to update
* @param	m2w matrix to update the aabb
* @return	the resulting aabb
*/
aabb aabb_from_aabb_and_transform(const aabb & aabb_1, mat4 m2w)
{
	aabb final_aabb;
		// For all three axes
	for (int i = 0; i < 3; i++) 
	{
	// Start by adding in translation
		final_aabb.min_point[i] = final_aabb.max_point[i] = m2w[3][i];
	// Form extent by summing smaller and larger terms respectively
		for (int j = 0; j < 3; j++) 
		{
			auto e = m2w[j][i] * aabb_1.min_point[j];
			auto f = m2w[j][i] * aabb_1.max_point[j];
			if (e < f) {
				final_aabb.min_point[i] += e;
				final_aabb.max_point[i] += f;
			} else {
				final_aabb.min_point[i] += f;
				final_aabb.max_point[i] += e;
			}
		}
	}

	return final_aabb;
}
/**
* @brief 	function to create bounding volume for sphere from points
* @param	points points to create the sphere
* @return	the resulting sphere
*/
sphere sphere_from_points_centroid(const std::vector<vec3>& points)
{
	if (!points.size())
		return sphere{};

	sphere final_sphere;

	aabb temp_aabb = aabb_from_points(points);
	//set center and radius
	final_sphere.center = temp_aabb.get_center();

	final_sphere.radius = glm::length(final_sphere.center - points[0]);
	
	for (unsigned i = 0; i < points.size(); i++)
	{
		auto temp_dist = glm::length(points[i] - final_sphere.center);
		if (temp_dist > final_sphere.radius)
			final_sphere.radius = temp_dist;
	}
	return final_sphere;
}
/**
* @brief 	function to compute the most distant points
* @param	min min point of the aabb
* @param	max max point of the aabb
* @param	pt points to create the aabb
*/
void MostSeparatedPointOnAABB(int & min, int & max, const std::vector<vec3> & pt)
{
	//Find what
	int minx = 0, maxx = 0, miny = 0, maxy = 0, minz = 0, maxz = 0;

	for (unsigned i = 1; i < pt.size(); i++)
	{
		if (pt[i].x < pt[minx].x) 
			minx = i;
		if (pt[i].x > pt[maxx].x) 
			maxx = i;
		if (pt[i].y < pt[miny].y)
			miny = i;
		if (pt[i].y > pt[maxy].y)
			maxy = i;
		if (pt[i].z < pt[minz].z)
			minz = i;
		if (pt[i].z > pt[maxz].z)
			maxz = i;
	}
	//Compute the distances from the points 

	float dist2x = glm::dot(pt[maxx] - pt[minx], pt[maxx] - pt[minx]);
	float dist2y = glm::dot(pt[maxy] - pt[miny], pt[maxy] - pt[miny]);
	float dist2z = glm::dot(pt[maxz] - pt[minz], pt[maxz] - pt[minz]);

	//Pick the points that are more distant
	//Pick the first ones as initial
	max = maxx;
	min = minx;
	//if y bigger
	if (dist2y >= dist2x && dist2y >= dist2z)
	{
		max = maxy;
		min = miny;
	}
	//if z bigger
	if (dist2z >= dist2x && dist2z >= dist2y)
	{
		max = maxz;
		min = minz;
	}
}
/**
* @brief 	function to compute a sphere from aabb
* @param	final_sphere the resulting sphere
* @param	points points to create the sphere
*/
void SphereFromDistantPoints(sphere & final_sphere, const std::vector<vec3>& points)
{
	//Finde the most separates points of the aabb
	int min, max;
	MostSeparatedPointOnAABB(min, max, points);

	//make the sphere cover this min and max points

	final_sphere.center = (points[min] + points[max]) * 0.5f;
	final_sphere.radius = glm::dot(points[max] - final_sphere.center, points[max] - final_sphere.center);
	final_sphere.radius = glm::sqrt(final_sphere.radius);
}
/**
* @brief 	function to update a sphere with a point
* @param	s sphere to update
* @param	point point to check if out or in the sphere
*/
void SphereOfSphereAndPt(sphere & sphere, const vec3 & point)
{
	//update sphere to include new point
	//compute distance to check if point is in or out
	vec3 d = point - sphere.center;
	//squared dist
	float dist2 = glm::dot(d, d);
	//check if outside
	if (dist2 > (sphere.radius * sphere.radius))
	{
		float dist = glm::sqrt(dist2);
		float nRadius = (sphere.radius + dist) * 0.5f;		
		float k = (nRadius - sphere.radius) / dist;
		sphere.radius = nRadius;
		sphere.center += d * k;
	}
}
/**
* @brief 	function to compute a sphere bounding volumen with ritter
* @param	points points to create the sphere
* @return	the resulting sphere
*/
sphere sphere_from_points_ritter(const std::vector<vec3>& points)
{
	if (!points.size())
		return sphere{};
	sphere final_sphere;
	SphereFromDistantPoints(final_sphere, points);

	//grow sphere for all the points to be inside
	for (unsigned i = 0; i < points.size(); i++)
		SphereOfSphereAndPt(final_sphere, points[i]);

	return final_sphere;
}

sphere sphere_from_points_pca(const std::vector<vec3>& points)
{
	return sphere();
}
/**
* @brief 	function to compute a sphere bounding volumen with iterative method
* @param	iteration_count the number of iterations to make
* @param	points points to create the sphere
* @param	shrink_ratio value to multiply each iteration
* @return	the resulting sphere
*/
sphere sphere_from_points_iterative(const std::vector<vec3>& points, int iteration_count, float shrink_ratio)
{
	
	sphere final_sphere = sphere_from_points_ritter(points);
	sphere tmp_sphere = final_sphere;
	//make a copy of the vector
	std::vector<vec3> tmp_points = points;

	for (int k = 0; k < iteration_count; k++)
	{
		//apply the shrink
		tmp_sphere.radius *= shrink_ratio;
		//Make the sphere data again
		for (unsigned i = 0; i < tmp_points.size(); i++)
		{
			SphereOfSphereAndPt(tmp_sphere, tmp_points[i]);
		}

		//if sphere obtained is smaller update it

		if (tmp_sphere.radius < final_sphere.radius)
			final_sphere = tmp_sphere;
	}

	return final_sphere;
}
