/**
* @file		mesh.cpp
* @date 	2/11/2019
* @author	Ivan Iglesias
* @par		Login: ivan.iglesias
* @par		Course: CS350
* @par		Assignment #2
* @brief 	Implementation of the mesh
*
* Hours spent on this assignment: 20h
*
*/
#include "pch.h"
#include "mesh.h"
#include <GLFW/glfw3.h>
#include <glad/glad.h>
//Load mesh from file
Mesh::Mesh(std::string file_name)
{
	//Load the objects and store it into the mesh
	Load_Obj(file_name, out_vertices, out_index_vert, out_normals);

	//Move objects to the center
	vec3 max_point;
    max_point.x = out_vertices[0].x;
	max_point.y = out_vertices[0].y;
	max_point.z = out_vertices[0].z;
	//Find maximum point
	for (unsigned i = 1; i < out_vertices.size(); i++)
	{		
		if (out_vertices[i].x >  max_point.x)
			max_point.x = out_vertices[i].x;
		if (out_vertices[i].y >  max_point.y)
			max_point.y = out_vertices[i].y;
		if (out_vertices[i].z >  max_point.z)
			max_point.z = out_vertices[i].z;
	}

	vec3 min_point;
	min_point.x = out_vertices[0].x;
	min_point.y = out_vertices[0].y;
	min_point.z = out_vertices[0].z;
	//Find maximum point
	for (unsigned i = 1; i < out_vertices.size(); i++)
	{		
		if (out_vertices[i].x < min_point.x)
			min_point.x = out_vertices[i].x;
		if (out_vertices[i].y < min_point.y)
			min_point.y = out_vertices[i].y;
		if (out_vertices[i].z < min_point.z)
			min_point.z = out_vertices[i].z;
	}

	auto mid_point = (max_point + min_point) / 2.0f;

	for (unsigned i = 0; i < out_vertices.size(); i++)
	{
		out_vertices[i] -= mid_point;		
	}

	auto max_total_value = glm::abs(max_point.x);
	
	if (max_total_value < glm::abs(max_point.y))
		max_total_value = glm::abs(max_point.y);
	else if (max_total_value < glm::abs(max_point.z))
		max_total_value = glm::abs(max_point.z);
	else if (max_total_value < glm::abs(min_point.x))
		max_total_value = glm::abs(min_point.x);
	else if (max_total_value < glm::abs(min_point.y))
		max_total_value = glm::abs(min_point.y);
	else if (max_total_value < glm::abs(min_point.z))
		max_total_value = glm::abs(min_point.z);

	for (unsigned i = 0; i < out_vertices.size(); i++)
	{
		out_vertices[i] /= max_total_value;
	}


	//Set the buffers
	SetBuffers();
}
//Set the buffers for opengl
void Mesh::SetBuffers()
{
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// VBOs
	// Position
	glGenBuffers(1, &positionBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, positionBufferObject);
	glBufferData(GL_ARRAY_BUFFER, out_vertices.size() * sizeof(glm::vec3), out_vertices.data(), GL_STATIC_DRAW);
	// Insert the VBO into the VAO
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);	

	glGenBuffers(1, &indexBufferObject);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, out_index_vert.size() * sizeof(unsigned short), out_index_vert.data(), GL_STATIC_DRAW);
	
	// Unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	
}
