/**
* @file		kdtree.cpp
* @date 	4/3/2019
* @author	Ivan Iglesias
* @par		Login: ivan.iglesias
* @par		Course: CS350
* @par		Assignment #4
* @brief 	Implementation of kdtree functions
*
* Hours spent on this assignment: 15h
*
*/
#include "kdtree.h"
#include <queue>
/**
* @brief 	function to call to build the tree
* @param	triangles vector of triangles of the mesh
* @depth	depth maximum depth for the tree
*/
void kdtree::build(std::vector<triangle> triangles, unsigned depth)
{
	clear_tree();
	//build aabb of the object
	auto max_aabb = aabb_from_triangles(triangles);
	max_depth = depth;
	compute_vectors(triangles);
	plane_limits init_plane_limits;
	//set the initial values for the limits
	init_plane_limits.limits_x[0] = init_plane_limits.limits_y[0] = init_plane_limits.limits_z[0] = 0;
	init_plane_limits.limits_x[1] = static_cast<int>(planes_x.size());
	init_plane_limits.limits_y[1] = static_cast<int>(planes_y.size());
	init_plane_limits.limits_z[1] = static_cast<int>(planes_z.size());
	//start colling recursively build
	m_root = build_recursive(triangles, max_aabb, 0, init_plane_limits);	
}
/**
* @brief 	cumpute the vectors of the planes by creating some sets ordering the values
* @param	triangles to create the planes with
*/
void kdtree::compute_vectors(std::vector<triangle>& triangles)
{
	std::set<float> planes_x_set;
	std::set<float> planes_y_set;
	std::set<float> planes_z_set;

	for (unsigned i = 0; i < triangles.size(); i++)
	{
		//x max
		planes_x_set.insert(std::max(std::max(triangles[i].points[0].x, triangles[i].points[1].x), triangles[i].points[2].x));
		//x min
		planes_x_set.insert(std::min(std::min(triangles[i].points[0].x, triangles[i].points[1].x), triangles[i].points[2].x));
		//y max
		planes_y_set.insert(std::max(std::max(triangles[i].points[0].y, triangles[i].points[1].y), triangles[i].points[2].y));
		//y min
		planes_y_set.insert(std::min(std::min(triangles[i].points[0].y, triangles[i].points[1].y), triangles[i].points[2].y));
		//z max
		planes_z_set.insert(std::max(std::max(triangles[i].points[0].z, triangles[i].points[1].z), triangles[i].points[2].z));
		//z min
		planes_z_set.insert(std::min(std::min(triangles[i].points[0].z, triangles[i].points[1].z), triangles[i].points[2].z));
	}
	//copy into the vectorss
	for (auto it = planes_x_set.begin(); it != planes_x_set.end(); it++)
		planes_x.push_back(*it);
	for (auto it = planes_y_set.begin(); it != planes_y_set.end(); it++)
		planes_y.push_back(*it);
	for (auto it = planes_z_set.begin(); it != planes_z_set.end(); it++)
		planes_z.push_back(*it);

}
/**
* @brief 	builds the tree recursively 
* @param	triangles to create the tree with
* @param	node_aabb aabb that cotains the bigger node
* @param	depth depth left to end the recursion
* @param	limits_planes values that limit the search in the planes
* @return	retruns the created node
*/
kdtree::KDNode * kdtree::build_recursive(std::vector<triangle> & triangles, aabb node_aabb, int depth, plane_limits limits_planes)
{	
	//Create last node
	if (depth == max_depth)
	{
		KDNode* last_node = new KDNode;
		last_node->child[0] = nullptr;
		last_node->child[1] = nullptr;
		last_node->triangles_list = triangles;
		last_node->node_aabb = node_aabb;
		return last_node;
	}
	float split_pos;
	int index_pos;
	auto modul = depth % 3;
	//call split and if not possible to split store last triangles and return
	if (!split_plane(node_aabb, triangles, modul, split_pos, limits_planes, index_pos))
	{
		KDNode* last_node = new KDNode;
		last_node->child[0] = nullptr;
		last_node->child[1] = nullptr;
		last_node->triangles_list = triangles;
		last_node->node_aabb = node_aabb;
		return last_node;
	}
	//compute the aabbs
	glm::vec3 max_pos(node_aabb.max_point);
	max_pos[modul] = split_pos;
	glm::vec3 min_pos(node_aabb.min_point);
	min_pos[modul] = split_pos;

	aabb aabb_left(node_aabb.min_point, max_pos);
	aabb aabb_right(min_pos, node_aabb.max_point);

	std::vector<triangle> triangles_list_right, triangles_list_left;

	//computes the mid plane
	plane mid_plane;
	glm::vec3 normal(0), pos(0);	
	normal[modul] = 1;
	pos[modul] = split_pos;

	mid_plane.set(normal, pos);

	KDNode * new_node = new KDNode;

	//divide triangles into left and right
	for (unsigned i = 0; i < triangles.size(); i++)
	{
		auto result_inter = intersection_plane_triangle(mid_plane, triangles[i]);
		if (result_inter == intersection_type::OUTSIDE)
			triangles_list_left.push_back(triangles[i]);
		else if (result_inter == intersection_type::INSIDE)
			triangles_list_right.push_back(triangles[i]);
		else if (result_inter == intersection_type::OVERLAPS) //if overlaps means it is in actual
			new_node->triangles_list.push_back(triangles[i]);
	}

	//create new node
	new_node->plane_position = split_pos;
	new_node->plane_axis = depth % 3;
	new_node->node_aabb = node_aabb;

	plane_limits plane_limits_left, plane_limits_right;

	plane_limits_left = limits_planes;
	plane_limits_right = limits_planes;

	//set the new plane limits
	switch (modul)
	{
	case 0:
		plane_limits_left.limits_x[1] = index_pos - 1;
		plane_limits_right.limits_x[0] = index_pos;
		break;
	case 1:
		plane_limits_left.limits_y[1] = index_pos - 1;
		plane_limits_right.limits_y[0] = index_pos;
		break;
	case 2:
		plane_limits_left.limits_z[1] = index_pos - 1;
		plane_limits_right.limits_z[0] = index_pos;
		break;
	default:
		break;
	}

	//call recursively to the childs
	if(triangles_list_left.size())
		new_node->child[0] = build_recursive(triangles_list_left, aabb_left, depth + 1, plane_limits_left);
	if (triangles_list_right.size())
		new_node->child[1] = build_recursive(triangles_list_right, aabb_right, depth + 1, plane_limits_right);

	return new_node;

}
/**
* @brief 	returns the tree in a vector sorted by breadth first
* @return	vector with all the nodes
*/
std::vector<kdtree::KDNode*> kdtree::get_breadth_first_sorted()
{
	
	std::queue<KDNode*> queue_nodes;	
	std::vector<KDNode*> nodes_result;
	queue_nodes.push(m_root);
	//use a queue to iterate the nodes
	while (!queue_nodes.empty())
	{//get the first node and insert its children
		auto node_front = queue_nodes.front();
		nodes_result.push_back(node_front);
		queue_nodes.pop();
		if(node_front->child[0] != nullptr)
			queue_nodes.push(node_front->child[0]);
		if (node_front->child[1] != nullptr)
			queue_nodes.push(node_front->child[1]);
	}
	return nodes_result;
}
/**
* @brief 	function that splits the planes 
* @param	triangles to split 
* @param	node_aabb aabb to split
* @param	split_pos position where to split
* @param	limits_planes values that limit the search in the planes
* @param	index_pos values that says where to split the vector
* @return	retruns if possible to split
*/
bool kdtree::split_plane(aabb node_aabb, std::vector<triangle> triangles, unsigned axis, float & split_pos, plane_limits limits_planes, int & index_pos)
{
	//compute the bounding volume left and right per plane
	std::vector<float> vector_using = planes_x;
	std::array<int, 2>& limits = limits_planes.limits_x;

	glm::vec3 normal(0);
	normal[axis] = 1;
	if (axis == 1) //select correct axiss
	{
		vector_using = planes_y;
		limits = limits_planes.limits_y;
	}
	else if (axis == 2)
	{
		vector_using = planes_z;
		limits = limits_planes.limits_z;
	}
	
	std::vector<float> heuristics_values;

	float best_heuristics_value = FLT_MAX;
	float best_plane_pos = 0;
	index_pos = 0;
	//iterate to find the best heuristic
	for(int i = limits[0]; i < limits[1]; i++)
	{
		//boundin volume right
		split_pos = vector_using[i];

		//construct plane with the value 
		glm::vec3 point;
		point[axis] = vector_using[i];
		plane actual_plane;
		actual_plane.set(normal, point);

		unsigned triangles_right = 0;
		unsigned triangles_left = 0;

		//divide triangles into left and right
		for (unsigned j = 0; j < triangles.size(); j++)
		{
			auto result_inter = intersection_plane_triangle(actual_plane, triangles[j]);
			if (result_inter == intersection_type::OUTSIDE)
				triangles_left++;
			else if (result_inter == intersection_type::INSIDE)
				triangles_right++;
		}
		glm::vec3 max_pos(node_aabb.max_point);
		max_pos[axis] = split_pos;
		glm::vec3 min_pos(node_aabb.min_point);
		min_pos[axis] = split_pos;
		//compute aabbs
		aabb aabb_left(node_aabb.min_point, max_pos);
		aabb aabb_right(min_pos, node_aabb.max_point);
		//compute heuristics		
		auto heuristics_value = heuristics(triangles_left, triangles_right, aabb_left, aabb_right, node_aabb);

		if (heuristics_value < best_heuristics_value)
		{
			best_heuristics_value = heuristics_value;
			best_plane_pos = split_pos;
			index_pos = i;
		}
	}
	//if heuristic value is bigger than triangle size then it is not worth to split
	if (best_heuristics_value >= triangles.size())
		return false;
	else
	{
		split_pos = best_plane_pos;
		return true;
	}	
}
/**
* @brief	compute the heuristic value
* @param	triangles_left_num number of triangle to the left
* @param	triangles_right_num number of triangles to the right
* @param	aabb_left aabb to compute surface
* @param	aabb_right aabb to compute surface
* @param	aabb_big big aabb to compute surface
* @return	retruns the  heuristic value
*/
float kdtree::heuristics(unsigned triangles_left_num, unsigned triangles_right_num, aabb aabb_left, aabb aabb_right, aabb aabb_big)
{
	return (aabb_left.compute_surface() * triangles_left_num + aabb_right.compute_surface() * triangles_right_num) / aabb_big.compute_surface();
}
/**
* @brief 	clear the tree
*/
void kdtree::clear_tree()
{
	if (m_root != nullptr)
	{
		clear_tree_recursive(m_root);
	}
	planes_x.clear();
	planes_y.clear();
	planes_z.clear();
	max_depth = 0;

}
/**
* @brief 	clear the tree recursively
* @param	KDNode to iterate
*/
void kdtree::clear_tree_recursive(KDNode * node)
{
	if (node == nullptr)
		return;
	if (node->child[0] != nullptr)
		clear_tree_recursive(node->child[0]);
	if (node->child[1] != nullptr)
		clear_tree_recursive(node->child[1]);

	delete node;
	node = nullptr;
}
/**
* @brief 	loads triangles from file
* @param	filename file to open and read
* @return	returns the triangle vector
*/
std::vector<triangle> load_triangles_from_obj(const char* filename)
{
	std::vector < glm::vec3 > out_vertices;
	std::vector < short unsigned > out_vert_index;
	std::vector < glm::vec3 > out_normals;

	Load_Obj(filename, out_vertices, out_vert_index, out_normals);

	//Move objects to the center
	vec3 max_point;
	max_point.x = out_vertices[0].x;
	max_point.y = out_vertices[0].y;
	max_point.z = out_vertices[0].z;
	//Find maximum point
	for (unsigned i = 1; i < out_vertices.size(); i++)
	{
		if (out_vertices[i].x >  max_point.x)
			max_point.x = out_vertices[i].x;
		if (out_vertices[i].y >  max_point.y)
			max_point.y = out_vertices[i].y;
		if (out_vertices[i].z >  max_point.z)
			max_point.z = out_vertices[i].z;
	}

	vec3 min_point;
	min_point.x = out_vertices[0].x;
	min_point.y = out_vertices[0].y;
	min_point.z = out_vertices[0].z;
	//Find maximum point
	for (unsigned i = 1; i < out_vertices.size(); i++)
	{
		if (out_vertices[i].x < min_point.x)
			min_point.x = out_vertices[i].x;
		if (out_vertices[i].y < min_point.y)
			min_point.y = out_vertices[i].y;
		if (out_vertices[i].z < min_point.z)
			min_point.z = out_vertices[i].z;
	}

	auto mid_point = (max_point + min_point) / 2.0f;

	for (unsigned i = 0; i < out_vertices.size(); i++)
	{
		out_vertices[i] -= mid_point;
	}

	
	auto max_total_value = glm::abs(max_point.x);

	if (max_total_value < glm::abs(max_point.y))
		max_total_value = glm::abs(max_point.y);
	else if (max_total_value < glm::abs(max_point.z))
		max_total_value = glm::abs(max_point.z);
	else if (max_total_value < glm::abs(min_point.x))
		max_total_value = glm::abs(min_point.x);
	else if (max_total_value < glm::abs(min_point.y))
		max_total_value = glm::abs(min_point.y);
	else if (max_total_value < glm::abs(min_point.z))
		max_total_value = glm::abs(min_point.z);

	for (unsigned i = 0; i < out_vertices.size(); i++)
	{
		out_vertices[i] /= max_total_value;
	}

	//insert triangles 3 points by 3
	std::vector<triangle> all_tri;
	for (unsigned i = 0; i < out_vert_index.size(); i += 3)
	{
		triangle tri;
		tri.points[0] = out_vertices[out_vert_index[i]];
		tri.points[1] = out_vertices[out_vert_index[i + 1]];
		tri.points[2] = out_vertices[out_vert_index[i + 2]];
		all_tri.push_back(tri);
	}
	return all_tri;
}