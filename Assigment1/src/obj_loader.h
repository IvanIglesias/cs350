/**
* @file		obj_loader.h
* @date 	2/11/2019
* @author	Ivan Iglesias
* @par		Login: ivan.iglesias
* @par		Course: CS350
* @par		Assignment #2
* @brief 	Implementation of obj loader functions
*
* Hours spent on this assignment: 20h
*
*/
#pragma once
#include <string>
#include <vector>
#include "pch.h"
void Load_Obj(std::string filename, std::vector < glm::vec3 > & out_vertices,
	std::vector < short unsigned > & out_vert_index,
	std::vector < glm::vec3 > & out_normals);