#!/usr/bin/env bash

# Make directories
mkdir .bin
mkdir dependencies
mkdir dependencies/lib
mkdir dependencies/include
mkdir dependencies/src
mkdir dependencies/src/imgui
mkdir .temp

# gtest
git clone https://github.com/abseil/googletest.git ./.temp/gtest
cd ./.temp/gtest/googletest
cmake -DBUILD_SHARED_LIBS=ON ./
cmake --build . --target gtest
cp ./lib/* ../../../dependencies/lib
cp -r ./include/* ../../../dependencies/include
cp -r ./bin/*.dll ../../../.bin
cd ../../../

# glm
git clone https://github.com/g-truc/glm.git ./.temp/glm
cp -r ./.temp/glm/glm ./dependencies/include

# glfw
git clone https://github.com/glfw/glfw.git ./.temp/glfw
cd ./.temp/glfw
cmake -DBUILD_SHARED_LIBS=ON -DCMAKE_BUILD_TYPE=Release ./
cmake --build . --target glfw
cp ./src/*.a ../../dependencies/lib
cp ./src/*.lib ../../dependencies/lib
cp ./src/*.dll ../../.bin
cp -r ./include/* ../../dependencies/include
cd ../../

# glad
git clone https://github.com/Dav1dde/glad ./.temp/glad
cd ./.temp/glad
cmake -DCMAKE_BUILD_TYPE=Release ./
cmake --build . --target glad
cp ./*.a ../../dependencies/lib
cp ./*.lib ../../dependencies/lib
cp ./*.dll ../../.bin
cp -r ./include/* ../../dependencies/include
cd ../../

# imgui
git clone https://github.com/ocornut/imgui ./.temp/imgui
cd ./.temp/imgui/
cp ./* ../../dependencies/src/imgui
cp ./examples/imgui_impl_opengl3.* ../../dependencies/src/imgui
cp ./examples/imgui_impl_glfw.* ../../dependencies/src/imgui
cd ../../

#
#rm -rf ./.temp